'use strict';

var singleUploadForm = document.querySelector('#singleUploadForm');
var singleFileUploadInput = document.querySelector('#singleFileUploadInput');
var singleFileUploadError = document.querySelector('#singleFileUploadError');
var singleFileUploadSuccess = document.querySelector('#singleFileUploadSuccess');

var multipleUploadForm = document.querySelector('#multipleUploadForm');
var multipleFileUploadInput1 = document.querySelector('#multipleFileUploadInput1');
var multipleFileUploadError = document.querySelector('#multipleFileUploadError');
var multipleFileUploadSuccess = document.querySelector('#multipleFileUploadSuccess');

function uploadSingleFile(file,str,refvalue,successCallback) {
//alert(refvalue);
    var formData = new FormData();
    formData.append("file", file);
    formData.append("documentDesc", str);
    formData.append('refNo', refvalue);
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "/uploadFile");

    xhr.onload = function() {
        console.log(xhr.responseText);
        var response = JSON.parse(xhr.responseText);
        if(xhr.status == 200) {
			successCallback && successCallback(response);
        } else {
            singleFileUploadSuccess.style.display = "none";
            singleFileUploadError.innerHTML = (response && response.message) || "Some Error Occurred";
        }
    }

    xhr.send(formData);
}



function uploadMultipleFiles(files, str,refvalue, upload_number,successCallback) {
//alert("reference value in multilple:" + refvalue);
  //debugger;
  var formData = new FormData();
  //for (var index = 0; index < files.length; index++) {
    //formData.append("files", files[index]);
  //}
  formData.append("files", files);
  formData.append("documentDesc", str);
    formData.append('refNo', refvalue);
    formData.append('uploadNumber', upload_number);
    

  var xhr = new XMLHttpRequest();
  xhr.open("POST", "/uploadMultipleFiles");

  xhr.onload = function() {
    console.log(xhr.responseText);
    var response = JSON.parse(xhr.responseText);
    if (xhr.status == 200) {
      successCallback && successCallback(response);
    } else {
      multipleFileUploadSuccess.style.display = "none";
      multipleFileUploadError.innerHTML = (response && response.message) || "Some Error Occurred";
    }
  }
  xhr.send(formData);
}
