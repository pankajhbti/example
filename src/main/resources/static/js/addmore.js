 
	jQuery.fn.addMore = function (className) {
		var no,snoId;
		$("." + className + ":eq(0)").clone()
									
									.find(":input, :radio ") // reset the form elements of new rows
										.each(function(index, domEle) {
											//alert('>>> ' + $(domEle) + " : " + $(domEle).attr("type") + " : " + domEle);
	
											// Increment 'name' & 'id' index for the dynamically adding elements
											if($(domEle).attr("name")) { // to deal with IE' HTMLUnknownElement error
												// increment  name & id values for new rows
												var inputName = $(domEle).attr("name");
												inputName = inputName.replace(/[\d]/, $("." + className).length);
												
												if(!isNaN(inputName.split('[')[1]))
													no=parseInt(inputName.split('[')[1])+1;
		                                          
												inputId = $(domEle).attr("id");
												
												if($(domEle).attr('class')=='sno frmelements'){
													inputId = inputId.split('_')[0]+'_'+inputId.split('_')[1]+'_'+no;//.substring(0,41)+no;
													
													snoId = inputId;
												}
												else{
													inputId = inputId.replace(/_\d_/, '_' + $("." + className).length + '_');
													 
												}
												
												$(domEle).attr("name", inputName).attr("id", inputId);
												// if radio button, reset the checked state
												if($(this).attr('type') == 'radio') {
													$(domEle).attr("checked", false);
													if (window.ActiveXObject) {// Fix For IE
														var new_ele = document.createElement('<input type="radio" name="'+ inputName +'"/>');
														new_ele.id = inputId;
														new_ele.value = domEle.value;
														var parent_node = domEle.parentNode; 
														parent_node.insertBefore(new_ele, domEle);
														parent_node.removeChild(domEle);
													}
												} else if($(this).attr('type') == 'select-one') {
													this.selectedIndex = 0;
												} else {
													// reset the value
													$(this).val('');
												}
											}
										})
										.end() // go up to clone() element
									.find("label")
										.each(function(index, domEle) {
											var labelFor = $(domEle).attr("for");
											labelFor = labelFor.replace(/_\d_/, '_' + $("." + className).length + '_');
											$(domEle).attr("for", labelFor);
										})
									.end() // go up to clone() element
									.find(".mandy").remove().end()
									.find("ul.errorMessage,.AddmoreError").remove().end()
									.insertBefore($(this).parent().parent()).end()
									.find("td:last").children(":last").after("&nbsp;&nbsp;<a class='removeLink' href='javascript: void(0)' onclick='removeMoreEle(this, &quot;"+ className +"&quot;)' title='Click here to remove'>X</a>");
									
									 // leave existing error messages;
									/*$("."+className).each(function(y){
										$(this).find(".removeAddMore").remove();
										if (y>0) {
											$(this).find("td:last").children(":last").after("<a class='removeAddMore' style='margin-left:5px;color:red;' href='javascript: void(0)' onclick='removeMoreEle(this, &quot;"+ className +"&quot;)' title='Click here to remove'>X</a>")
										}
									});*/
		
									if (className=="hostelWardenDetails" || className=="hostelGaurdDetails" || className=="msjeDrugsFundingDetails" 
										|| className=="msjeIpopFundingDetails" || className=="msjeScFundingDetails" || className=="projectSiteEntries"
											 || className=="staffDetailsEntries"  || className=="peerDetailsEntries"  || className=="trainingDetailsEntries" || className=="beneficiaryDetailsEntries"  || className=="compoManagingBody" || className=="sourceFundingList" || className=="qualificationDoctorStaff" || className=="detailsOfEmployeeList")
										$("#"+snoId).val(no);
									if (className=="form2ContVisitOfOfficials") setCalendarsForSimilarTypes("].officersDateVisit");
									if (className=="form2ContVisitByDoctor") setCalendarsForSimilarTypes("].doctorDateVisit");
									
									if (className=="grantsReceived") setCalendarsForSimilarTypes("].date");
									
									if (className=="inmatesCopy") setCalendarsForSimilarTypes("].admissionDate");
									if (className=="teachersPerticulars") setCalendarsForSimilarTypes("].dateOfJoining");
									if(className=="counsellorsCopy") {
									  setCalendarsForSimilarTypes("].joiningDate");
									  setCalendarsForSimilarTypes("].relievingDate");  
									}
									if (className=="staffDetails") setCalendarsForSimilarTypes("].appointmentDate");
									//$("."+className).next(".removeLink").remove();
									//$("."+className).each(function(i){if(i>0)$(this).find(".mandy").remove();});// removing mandatory asterisks
									setCalendarsForSimilarTypes("].fromPeriodOfWorkDone");
									setCalendarsForSimilarTypes("].sanctionDate");
									setCalendarsForSimilarTypes("].toPeriodOfWorkDone");
									setCalendarsForSimilarTypes("].fromPeriodOfWorkDoneAlco");
									setCalendarsForSimilarTypes("].toPeriodOfWorkDoneAlco");
								    setCalendarsForSimilarTypes("].releaseDate");
								    
								    setCalendarsForSimilarTypesDisabledFutureDates("].dateOfVideo");
								    setCalendarsForSimilarTypesDisabledFutureDates("].dateOfPhoto");
								    

								    setCalendarsForSimilarTypesDisabledFutureDates("].phaseInDate");
								    setCalendarsForSimilarTypesDisabledFutureDates("].phaseOutDate");
								    setCalendarsForSimilarTypesDisabledFutureDates("].dateWorkingFrom");
								    setCalendarsForSimilarTypesDisabledFutureDates("].dateOfJoining");
								    setCalendarsForSimilarTypesDisabledFutureDates("].dateOfLeaving");
								    setCalendarsForSimilarTypesDisabledFutureDates("].dateOfVisit");
								    setCalendarsForSimilarTypesDisabledFutureDates("].fromDate");
								    setCalendarsForSimilarTypesDisabledFutureDates("].toDate");
									 
									
									var j=1;
									$(".msjeIpopFundingDetails").find(".doctype").each(function(){
									 this.id="file"+j;
									  j++;
									});
									
									var p=1;
									$(".msjeIpopFundingDetails").find(".butId").each(function(){
									 this.id=p;
									  p++;
									});
									
									var k=1;
									$(".msjeIpopFundingDetails").find(".docdesc").each(function(){
									 this.id="documentDesc"+k;
									  k++;
									});
									
									var n=1;
									$(".msjeIpopFundingDetails").find(".otherFiles").each(function(){
									 this.id="otherFiles"+n;
									  n++;
									});
									 n=n-1;
									$('#otherFiles'+n).hide();
									var l=1;
									$(".msjeIpopFundingDetails").find(".viewOtherfile").each(function(){
									 this.id="viewOtherfile"+l;
									  l++;
									});
									
									
									
									
		return this; // to maintain the jQuery chaining
	};
	
	
	
	 function isNumberKey(evt){
		    var charCode = (evt.which) ? evt.which : event.keyCode
		    if (charCode > 31 && (charCode < 48 || charCode > 57))
		    	{		    	
		    	 return false;
		    	}		       
		    return true;
		}
	
	
	jQuery.fn.addMoreLocations = function (className) {
		if(className=='exPostFactoSanction'){
			if($("."+className).length >= 3){
			    alert("Ex-post Facto sanction details are already added for past 3 years.");
			   return false;
			   }
		}
		var cloneElement = $("." + className + ":eq(0)").clone()
									
									.find(":input, :radio, :tr ,:a") // reset the form elements of new rows
										.each(function(index, domEle) {
											//alert('>>> ' + $(domEle) + " : " + $(domEle).attr("type") + " : " + domEle);
										
											// Increment 'name' & 'id' index for the dynamically adding elements
											if($(domEle).attr("name")) { // to deal with IE' HTMLUnknownElement error
												// increment  name & id values for new rows
												var inputName = $(domEle).attr("name");
												inputName = inputName.replace(/[\d]/, $("." + className).length);
												
												var inputId = $(domEle).attr("id");
												//alert(inputId);
												inputId = inputId.replace(/_\d_/, '_' + $("." + className).length + '_');
												
												$(domEle).attr("name", inputName)
														.attr("id", inputId);
												
												// if radio button, reset the checked state
												if($(this).attr('type') == 'radio') {
													$(domEle).attr("checked", false);
													if (window.ActiveXObject) {// Fix For IE
														var new_ele = document.createElement('<input type="radio" name="'+ inputName +'"/>');
														new_ele.id = inputId;
														new_ele.value = domEle.value;
														var parent_node = domEle.parentNode; 
														parent_node.insertBefore(new_ele, domEle);
														parent_node.removeChild(domEle);
													}
												} else if($(this).attr('type') == 'select-one') {
													this.selectedIndex = 0;
												} else {
													// reset the value
													$(this).val('');
												}
											}
										})
										.end() // go up to clone() element
									.find("label")
										.each(function(index, domEle) {
											var labelFor = $(domEle).attr("for");
											labelFor = labelFor.replace(/_\d_/, '_' + $("." + className).length + '_');
											$(domEle).attr("for", labelFor);
										})
										.end(); // go up to clone() element
									if(className == 'exPostFactoSanction')	{
										$(cloneElement).find(".pastLiability").each(function(i) {
											if (i>0) $(this).remove();
											if(i==0) $(this).find(".insNo").val("1");
										});
										var cloneElementAgain = cloneElement.insertAfter($(".exPostFactoSanction:last"));
									}else
									 	var cloneElementAgain = cloneElement.insertBefore($(this).parent().parent());
										
										cloneElementAgain.find(".locationHead").each (function (){
										var len = $("." + className).length;
										$(this).html("<b>Location"+len+" Details:</b>&nbsp;&nbsp;<a class='removeLink' href='javascript: void(0)' onclick='removeMoreEle(this, &quot;"+ className +"&quot;)' title='Click here to remove location'>Remove Location</a>");
									})
									.end()
									.find(".sanghasHead").each (function (){
										var len = $("." + className).length;
										$(this).html("<b>Location"+len+" Details:</b>&nbsp;&nbsp;<a class='removeLink' href='javascript: void(0)' onclick='removeMoreEle(this, &quot;"+ className +"&quot;)' title='Click here to remove location'>Remove Location</a>");
									})								
									.end()
									.find(".historyHead").each (function (){
										var len = $("." + className).length;
										$(this).html("<b>Sanction"+len+" Details:</b>&nbsp;&nbsp;<a class='removeLink' href='javascript: void(0)' onclick='removeMoreEle(this, &quot;"+ className +"&quot;)' title='Click here to sanction history'>Remove Sanction History</a>");
									})
									.end()
									.find(".exPostHead").each (function (){
										var len = $("." + className).length;
										$(this).html("<b>Ex-post Facto Sanction"+len+" Details:</b>&nbsp;&nbsp;<a class='removeLink' href='javascript: void(0)' onclick='removeMoreEle(this, &quot;"+ className +"&quot;)' title='Click here to add ex-post facto sanction'>Remove Ex-post Facto Sanction </a>");
									})								
									.end()
									.find("ul.errorMessage,.AddmoreError").remove().end() // leave existing error messages;
									.find(".programmeName").attr("style","display:none;");
									
									setLocationHeadingNo();
									setCalendarsForSimilarTypes("].proposedDateOfCamps");
									setCalendarsForSimilarTypes("].orderDate");
									setCalendarsForSimilarTypes("].sanctionDate");
									setCalendarsForSimilarTypes("].releaseDate");
									
		return this; // to maintain the jQuery chaining
	};
	
	
	//addmore with in addmore
	jQuery.fn.addMoreInner = function (anchrObj, className) {
	var eleLength = 0;
	
		$(this).parent().parent().parent().find("."+className+":first").clone()
									
									.find(":input, :radio ") // reset the form elements of new rows
										.each(function(index, domEle) {
											//alert('>>> ' + $(domEle) + " : " + $(domEle).attr("type") + " : " + domEle);
	
											// Increment 'name' & 'id' index for the dynamically adding elements
											if($(domEle).attr("name")) { // to deal with IE' HTMLUnknownElement error
												// increment  name & id values for new rows
												eleLength = $(anchrObj).parent().parent().parent().find("."+className).length;
												var inputName = $(domEle).attr("name");
												eleLength = eleLength.toString().split("").reverse().join("");
												eleLength = parseInt(eleLength);
												inputName = inputName.split("").reverse().join("").replace(/[\d]/, eleLength).split("").reverse().join("");
												
		        								
												var inputId = $(domEle).attr("id");
												inputId = inputId.split("").reverse().join("").replace(/_\d_/, '_' + eleLength + '_').split("").reverse().join("");
												
												$(domEle).attr("name", inputName)
														.attr("id", inputId);
												
											  
											
											  // $(domEle).val(eleLength+1);
											// if radio button, reset the checked state
												if($(this).attr('type') == 'radio') {
													$(domEle).attr("checked", false);
													if (window.ActiveXObject) {// Fix For IE
														var new_ele = document.createElement('<input type="radio" name="'+ inputName +'"/>');
														new_ele.id = inputId;
														new_ele.value = domEle.value;
														var parent_node = domEle.parentNode; 
														parent_node.insertBefore(new_ele, domEle);
														parent_node.removeChild(domEle);
													}
												} else if($(this).attr('type') == 'select-one') {
													this.selectedIndex = 0;
												} else {
													// reset the value
													$(this).val('');
												}
											}
										})
										.end() // go up to clone() element
									.find("label")
										.each(function(index, domEle) {
											var labelFor = $(domEle).attr("for");
											labelFor = labelFor.replace(/_\d_/, '_' + $("." + className).length + '_');
											$(domEle).attr("for", labelFor);
										})
									.end() // go up to clone() element
									.find(".mandy").remove().end()
									.find("ul.errorMessage,.AddmoreError").remove().end()
									.insertBefore($(this).parent().parent()).end()
									.find("td:last").children(":last").after("&nbsp;&nbsp;<a class='removeLink' href='javascript: void(0)' onclick='removeMoreEleInner(this, &quot;"+ className +"&quot;)' title='Click here to remove'>X</a>");
									
									 // leave existing error messages;
									/*$("."+className).each(function(y){
										$(this).find(".removeAddMore").remove();
										if (y>0) {
											$(this).find("td:last").children(":last").after("<a class='removeAddMore' style='margin-left:5px;color:red;' href='javascript: void(0)' onclick='removeMoreEle(this, &quot;"+ className +"&quot;)' title='Click here to remove'>X</a>")
										}
									});*/
	
									if (className=="form2ContVisitOfOfficials") setCalendarsForSimilarTypes("].officersDateVisit");
									if (className=="form2ContVisitByDoctor") setCalendarsForSimilarTypes("].doctorDateVisit");
	
									if (className=="grantsReceived") setCalendarsForSimilarTypes("].date");
									if (className=="inmatesCopy") setCalendarsForSimilarTypes("].admissionDate");
									if (className=="teachersPerticulars") setCalendarsForSimilarTypes("].dateOfJoining");
									if(className=="counsellorsCopy") {
									  setCalendarsForSimilarTypes("].joiningDate");
									  setCalendarsForSimilarTypes("].relievingDate");  
									}
									if (className=="staffDetails") setCalendarsForSimilarTypes("].appointmentDate");
									setCalendarsForSimilarTypes("].releaseDate");
									//$(anchrObj).parent().parent().parent().find(".insNo:last").val(eleLength+parseInt(1));
									var i = 1;
									$(anchrObj).parent().parent().parent().find(".insNo").each(function(){
									  $(this).val(i);
									  i++;
									})
									
									//$("."+className).next(".removeLink").remove();
									//$("."+className).each(function(i){if(i>0)$(this).find(".mandy").remove();});// removing mandatory asterisks
									
		return this; // to maintain the jQuery chaining
	};
	
	
	
	function removeMoreEle(ele, classNameEleToModify) {
		var no,snoId;
	    var parentEle = $(ele).parent().parent().parent();
		$(ele).parents("."+classNameEleToModify).remove();
		$("."+classNameEleToModify).each(function(i){
			$(this).find(":input, :radio").each(function(index, domEle) {
				if($(domEle).attr("name")) { // to deal with IE' HTMLUnknownElement error
					// increment  name & id values for new rows
					var inputName = $(domEle).attr("name");
					//inputName = inputName.split("").reverse().join("").replace(/\d+/, i).split("").reverse().join("");
					
					inputName = inputName.replace(/\d+/, i);
					
					if(!isNaN(inputName.split('[')[1]))
						no=parseInt(inputName.split('[')[1])+1;
					var inputId = $(domEle).attr("id");
					if($(domEle).attr('class')=='sno frmelements'){
						inputId = inputId.split('_')[0]+'_'+inputId.split('_')[1]+'_'+no;//.substring(0,41)+no;
						snoId = inputId;
					}
					else{
						inputId = inputId.replace(/_\d+_/, '_' + i + '_');
					}
				//	inputId = inputId.split("").reverse().join("").replace(/_\d+_/, '_' + i + '_').split("").reverse().join("");
					
				//	inputId = inputId.replace(/\d+/, i);
					
					$(domEle).attr("name", inputName).attr("id", inputId);
					
					if ((classNameEleToModify=="hostelWardenDetails" || classNameEleToModify=="hostelGaurdDetails" || classNameEleToModify=="projectSiteEntries"
						 || classNameEleToModify=="staffDetailsEntries"  || classNameEleToModify=="peerDetailsEntries"  || classNameEleToModify=="trainingDetailsEntries" || classNameEleToModify=="beneficiaryDetailsEntries"
						|| classNameEleToModify=="msjeDrugsFundingDetails" || classNameEleToModify=="msjeIpopFundingDetails" || classNameEleToModify=="msjeScFundingDetails" || classNameEleToModify=="compoManagingBody" 
							|| classNameEleToModify=="sourceFundingList" || classNameEleToModify=="qualificationDoctorStaff" || classNameEleToModify=="detailsOfEmployeeList") && !isNaN(inputName.split('[')[1])) $("#"+snoId).val(no);
					
					if($(this).attr('type') == 'radio') {
						if (window.ActiveXObject) {// Fix For IE
							var new_ele = null;
							if (domEle.checked)
								new_ele = document.createElement('<input type="radio" name="'+ inputName +'" checked="checked"/>');
							else 
								new_ele = document.createElement('<input type="radio" name="'+ inputName +'"/>');
							new_ele.id = inputId;
							new_ele.value = domEle.value;
							var parent_node = domEle.parentNode; 
							parent_node.insertBefore(new_ele, domEle);
							parent_node.removeChild(domEle);
						}
					}
				}
			});
		});
		
		
		if(classNameEleToModify=="grantInAidReceived")
		{
			calculateTotalCost();
			calculateTotalGrant();
			calculateTotalEstimate();
			 
		}	
		if(classNameEleToModify == "grantInAidReceivedNonCamp")
		{
			calculateTotalCostNonCamp();
			calculateTotalAppliedNonCamp();
			calculateTotalBenefiNonCamp();
		}
		
		if( classNameEleToModify == "aidAndAppliancesDistributed")
		{
			calculateBeneficiaries();
			calculateMobilityAids();
			calculateOrthoticDevices();
			calculateHearingAids();
			calculateAssistiveDevices();
			calculateRelatedAssistiveDevices();
			calculateCorrectiveSurgeries();
		}
		
		if(classNameEleToModify == "calculationTable")
		{
			totalRecmAmt();
		}
		
		if(classNameEleToModify == "calculationTableNewCases")
		{
			totalRecmAmtNewCases();
		}


		$("."+classNameEleToModify).each(function(i){
				$(this).find("label").each(function(){
					var labelFor = $(this).attr("for");
					labelFor = labelFor.replace(/_\d_/, '_' + i + '_');
					$(this).attr("for", labelFor);
				});
		});
		
		if (classNameEleToModify=="addressLocation" || classNameEleToModify=="history" || classNameEleToModify=="exPostFactoSanction") {
			setLocationHeadingNo();
			setCalendarsForSimilarTypes("].proposedDateOfCamps");
			setCalendarsForSimilarTypes("].orderDate");
			try {
				setLoationLen();
			} catch (e) {
			}
		}
		 
		if (classNameEleToModify=="form2ContVisitOfOfficials") setCalendarsForSimilarTypes("].officersDateVisit");
		if (classNameEleToModify=="form2ContVisitByDoctor") setCalendarsForSimilarTypes("].doctorDateVisit");
		
		if (classNameEleToModify=="grantsReceived") setCalendarsForSimilarTypes("].date");
		if (classNameEleToModify=="inmatesCopy") setCalendarsForSimilarTypes("].admissionDate");
		if (classNameEleToModify=="teachersPerticulars") setCalendarsForSimilarTypes("].dateOfJoining");
		if(classNameEleToModify=="counsellorsCopy") {
									  setCalendarsForSimilarTypes("].joiningDate");
									  setCalendarsForSimilarTypes("].relievingDate");  
									}
		setCalendarsForSimilarTypes("].sanctionDate");	
		setCalendarsForSimilarTypes("].releaseDate");	
							
	   if (classNameEleToModify=="staffDetails") setCalendarsForSimilarTypes("].appointmentDate");
	   clacAllTotalForRemove('.itemwiseCalc', '.itemwiseTotal');
	   clacAllTotalForRemove('.subTotal1', '.subTotals');
	   clacAllTotalForRemove('.subTotals','.grandTotal');
	   
	   var i = 1;
		$(parentEle).find(".insNo").each(function(){
		 $(this).val(i);
		  i++;
		});
		
		var j = 1;
		$(".msjeIpopFundingDetails").find(".doctype").each(function(){
		 this.id="file"+j;
		 j++;
		});
		var p=1;
			$(".msjeIpopFundingDetails").find(".butId").each(function(){
			 this.id=p;
			  p++;
			});
			
			var k=1;
					$(".msjeIpopFundingDetails").find(".docdesc").each(function(){
						 this.id="documentDesc"+k;
						  k++;
						});
						var n=1;
									$(".msjeIpopFundingDetails").find(".otherFiles").each(function(){
									 this.id="otherFiles"+n;
									  n++;
									});
									
									var l=1;
									$(".msjeIpopFundingDetails").find(".viewOtherfile").each(function(){
									 this.id="viewOtherfile"+l;
									  l++;
									});
	}
	
	
	function removeMoreEleInner(ele, classNameEleToModify) {
	 	var parentEle = $(ele).parent().parent().parent();
		$(ele).parents("."+classNameEleToModify).remove();
		$(parentEle).find("."+classNameEleToModify).each(function(i){
			var eleLength = i.toString().split("").reverse().join("");
			eleLength = parseInt(eleLength);
			$(this).find(":input, :radio").each(function(index, domEle) {
				if($(domEle).attr("name")) { // to deal with IE' HTMLUnknownElement error
					// increment  name & id values for new rows
					var inputName = $(domEle).attr("name");
					inputName = inputName.split("").reverse().join("").replace(/\d+/, eleLength).split("").reverse().join("");
					
					//inputName = inputName.replace(/\d+/, i);
					var inputId = $(domEle).attr("id");
					inputId = inputId.split("").reverse().join("").replace(/_\d+_/, '_' + eleLength + '_').split("").reverse().join("");
					//inputId = inputId.replace(/_\d+_/, '_' + i + '_');
					
					$(domEle).attr("name", inputName).attr("id", inputId);
					
					if($(this).attr('type') == 'radio') {
						if (window.ActiveXObject) {// Fix For IE
							var new_ele = null;
							if (domEle.checked)
								new_ele = document.createElement('<input type="radio" name="'+ inputName +'" checked="checked"/>');
							else 
								new_ele = document.createElement('<input type="radio" name="'+ inputName +'"/>');
							new_ele.id = inputId;
							new_ele.value = domEle.value;
							var parent_node = domEle.parentNode; 
							parent_node.insertBefore(new_ele, domEle);
							parent_node.removeChild(domEle);
						}
					}
				}
			});
		});
		$("."+classNameEleToModify).each(function(i){
				$(this).find("label").each(function(){
					var labelFor = $(this).attr("for");
					labelFor = labelFor.replace(/_\d_/, '_' + i + '_');
					$(this).attr("for", labelFor);
				});
		});
		
	 setCalendarsForSimilarTypes("].releaseDate");
		var i = 1;
		$(parentEle).find(".insNo").each(function(){
		 $(this).val(i);
		  i++;
		})
		var total2=0;
		$(parentEle).find(".subTotal2").each(function(){
		total2 += parseInt($(this).val());
		  })
		$(parentEle).parent().parent().parent().next().find(".subTotals").val(total2);  
		clacAllTotalForRemove('.subTotals','.grandTotal');
	}
	
	
	
	
	function clacAllTotalForRemove(srcSelector,trgtSelector){
		var totalRoomNum = Number(0);
		$(srcSelector).each(function() {
			var roomNum = $(this).val();
			roomNum = roomNum.replace(/,/g, "");
			if(roomNum != '' && isFinite(roomNum))
				totalRoomNum += parseFloat(roomNum);
		});
		
		if(totalRoomNum == "0")
		{
		totalRoomNum = "";
		}
		$(trgtSelector).val(totalRoomNum);
		
		$('.currency').each(function(i){
			$("#"+$(this).attr("id")).formatCurrency();
	    });
		return this; 
	}
	
	
	//--------- Number adding script ---------
	//TEST: Tested for class selector; TODO: test for remaining selector
	/**
	 * Calculate the sum of the all source elements values and 
	 * updates the target element value with it. 
	 * 
	 * Source elements are identified by using only one jQuery selector
	 */
	jQuery.fn.calcAllTotal = function (srcSelector, trgtSelector) {
	
		var totalRoomNum = Number(0);
		
		$(srcSelector).each(function() {
			var roomNum = $(this).val();
			roomNum = roomNum.replace(/,/g, "");
			if(roomNum != '' && isFinite(roomNum))
				totalRoomNum += parseFloat(roomNum);
		});
		
		$(trgtSelector).val(totalRoomNum);
				
		return this; // to maintain the jQuery chaining
	};
	
	/*calculates total general male of residential*/
	/*jQuery.fn.calcAllTotalGeneralMale = function (srcSelector, trgtSelector,maletotal) {
		
		var totalRoomNum = Number(0);
	
		$(srcSelector).each(function() {
			var roomNum = $(this).val();
			roomNum = roomNum.replace(/,/g, "");
			if(roomNum != '' && isFinite(roomNum))
				totalRoomNum += parseFloat(roomNum);
		});
			
		$(trgtSelector).val(totalRoomNum);
		$(maletotal).val(totalRoomNum);
		$(grand_total_male).val(grand_total);
		
		return this; // to maintain the jQuery chaining
	};*/
	
	/*calculates total general female of residential*/
	
/*	jQuery.fn.calcAllTotalGeneralFemale = function (srcSelector,genfemale, trgtSelector,femaletotal) {
		
		var totalRoomNum = Number(0);
		var totalGenFemale = $(genfemale).val().replace(/,/g,"").trim()==""?0:$(genfemale).val().replace(/,/g, "");
		
		$(srcSelector).each(function() {
			var roomNum = $(this).val();
			roomNum = roomNum.replace(/,/g, "");
			if(roomNum != '' && isFinite(roomNum))
				totalRoomNum += parseFloat(roomNum)+parseFloat(totalGenFemale);
		});			
		$(trgtSelector).val(totalRoomNum);
		$(femaletotal).val(totalGenFemale);
			
		return this; // to maintain the jQuery chaining
	};*/
	
/*jQuery.fn.calcAllTotalGeneralTrans = function (srcSelector,genmale,genfemale, trgtSelector,transtotal) {
		
		var totalRoomNum = Number(0);
		var totalgenmale = $(genmale).val().replace(/,/g,"").trim()==""?0:$(genmale).val().replace(/,/g, "");
		var totalGenFemale = $(genfemale).val().replace(/,/g,"").trim()==""?0:$(genfemale).val().replace(/,/g, "");
	
		$(srcSelector).each(function() {
			var roomNum = $(this).val();		
			roomNum = roomNum.replace(/,/g, "");
			if(roomNum != '' && isFinite(roomNum))
				totalRoomNum += parseFloat(roomNum)+parseFloat(totalgenmale)+parseFloat(totalGenFemale);
			$(transtotal).val(roomNum);
		});		
				
		$(trgtSelector).val(totalRoomNum);
		
			
		return this; // to maintain the jQuery chaining
	};*/
	
	
	
	
	
	
	
	
	jQuery.fn.calcAllFloatTotal = function (srcSelector, trgtSelector) {
		var totalRoomNum = Number(0);
		$(srcSelector).each(function() {
			var roomNum = $(this).val();
			roomNum = roomNum.replace(/,/g, "");
			if(roomNum != '' && isFinite(roomNum))
				totalRoomNum += parseFloat(roomNum);
		});
		
		if(totalRoomNum == "0")
		{
		totalRoomNum = "";
		}
		$(trgtSelector).val(totalRoomNum);
		
		$('.currency').each(function(i){
			$("#"+$(this).attr("id")).formatCurrency();
	    });
		return this; // to maintain the jQuery chaining
	};
	
/*	jQuery.fn.calcGeneralTotal = function (srcSelector,trgtSelector){	
	
		var num = Number(0);
		alert("coming here"+trgtSelector);
		var genmale = $(general).val().replace(/,/g, "").trim()==""?0:$(general).val().replace(/,/g, "");
		var genfemale = $(schedules_caste).val().replace(/,/g, "").trim()==""?0:$(schedules_caste).val().replace(/,/g, "");
		var gentrans = $(schedules_tribes).val().replace(/,/g, "").trim()==""?0:$(schedules_tribes).val().replace(/,/g, "");
		num = (parseInt(genmale)+parseInt(genmale)+parseInt(gentrans));
		$(trgtSelector).val(num);
		
		return this;
	} 
	*/
	jQuery.fn.calcAllDifference = function (firstVal, secondVal, trgtSelector) {
		//alert(first+"firstVal");
		//alert(second+"secondVal");
		var num = Number(0);	
		var first = $(firstVal).val().replace(/,/g, "").trim()==""?0:$(firstVal).val().replace(/,/g, "");
		var second = $(secondVal).val().replace(/,/g, "").trim()==""?0:$(secondVal).val().replace(/,/g, "");	
		num = (parseInt(first) - parseInt(second))<0?(parseInt(first) - parseInt(second))*parseInt(-1):parseInt(first) - parseInt(second);
		$(trgtSelector).val(num);
		//alert(num+"diff");
		return this; // to maintain the jQuery chaining
	};
	
	/**
	 * Calculate the sum of the source elements values and 
	 * updates the target element value with it. 
	 * 
	 * Source element selectors need to give for all the individual elements, in array format
	 */
	function calcTotal(ele, srcArr, trgtSel) {
		var total = Number(0);
		//$(ele).parent().parent().css("backgroundColor", "red");
		var inputArr = $("input", $(ele).parent().parent());
	
		for(i = 0; i < srcArr.length; i++) {
			var val = $(inputArr[srcArr[i]-1]).val();
			if(val != '' && isFinite(val)) {
				total += parseInt(val);
			} else {
				total="";
				break;
			}
		}
		
		$(inputArr[trgtSel-1]).val(total);
		
	//	alert(inputArr.length);
	//	for(i = 0; i < inputArr.length; i++) {
	//	}
		
		return this; // to maintain the jQuery chaining
	};
	
	/**
	 * Calculates the float total 
	 * @param ele
	 * @param srcArr
	 * @param trgtSel
	 * @return
	 */
	function calcFloatTotal(ele, srcArr, trgtSel) {
		var total = Number(0);
		//$(ele).parent().parent().css("backgroundColor", "red");
		var inputArr = $("input", $(ele).parent().parent());
	
		for(i = 0; i < srcArr.length; i++) {
			var val = $(inputArr[srcArr[i]-1]).val();
			if(val != '' && isFinite(val)) {
				total += parseFloat(val);
			} else {
				total="";
				break;
			}
		}
		
		$(inputArr[trgtSel-1]).val(total);
		
	//	alert(inputArr.length);
	//	for(i = 0; i < inputArr.length; i++) {
	//	}
		
		return this; // to maintain the jQuery chaining
	};
	
	//--- Util functions ---
	function getKeyCode(e)
	{
		if (navigator.appName == "Microsoft Internet Explorer")
		{
			return event.keyCode; // not a typo; it is keyCode only
		}
		return e.which;
	}
	
	function currencyFormatterAndCalcTotal(thisObj, thisClassName, totalObjClassName) {
		$(thisObj).formatCurrency();
		if (totalObjClassName!="") {
	
			$(thisObj).calcAllFloatTotal('.'+thisClassName , '.'+totalObjClassName);
		}
		$('.currency').each(function(i){
			$("#"+$(this).attr("id")).formatCurrency();
	    });
	}
	/**
	 * This function is used for setting the heading number for location 
	 */
	function setLocationHeadingNo() {
		
		$(".locationHead").each(function (i) {
			var anchrele = $(this).find("a");		
			$(this).html("<b>Location"+ (i+1) +":</b>&nbsp;&nbsp;").append(anchrele);
		});
		$(".sanghasHead").each(function (i) {
			var anchrele = $(this).find("a");		
			$(this).html("<b>Location"+ (i+1) +":</b>&nbsp;&nbsp;").append(anchrele);
		});
		
		$(".historyHead").each(function (i) {
			
			var anchrele = $(this).find("a");
			$(this).html("<b>Sanction"+ (i+1) +":</b>&nbsp;&nbsp;").append(anchrele);
		});
		$(".exPostHead").each(function (i) {
			
			var anchrele = $(this).find("a");
			$(this).html("<b>Ex-post Facto Sanction"+ (i+1) +":</b>&nbsp;&nbsp;").append(anchrele);
		});
		
	}
	
	function setCalendarsForSimilarTypes(typeVal) {
		if (typeVal!=""){
			$("input[name$='"+typeVal+"']").each(function (i){
				
				$(this).removeClass("hasDatepicker");
				$(this).nextAll().each(function(){
					if ($(this).is("img")) $(this).remove();
				});
				
				$(this).datepicker({
					showOn: "both",
					hideIfNoPrevNext: true,
					dateFormat: "dd/mm/yy",
					defaultDate: currentDate,
					changeMonth: true,
					changeYear: true,
					buttonImageOnly: true,
					buttonText: "Open",
					buttonImage: "images/datepicker/calendar.gif",
					yearRange: '1900:2050'
				});
			});
		}
	}
	
	function setCalendarsForSimilarTypesDisabledFutureDates(typeVal) {
		if (typeVal!=""){
			$("input[name$='"+typeVal+"']").each(function (i){
				
				$(this).removeClass("hasDatepicker");
				$(this).nextAll().each(function(){
					if ($(this).is("img")) $(this).remove();
				});
				
				$(this).datepicker({
					showOn: "both",
					hideIfNoPrevNext: true,
					dateFormat: "dd/mm/yy",
					defaultDate: currentDate,
					changeMonth: true,
					changeYear: true,
					buttonImageOnly: true,
					maxDate: "today",
					buttonText: "Open",
					buttonImage: "images/datepicker/calendar.gif",
					yearRange: '1900:2050'
				});
			});
		}
	}
	
	function setCalendarComponent(calEle){
		$(calEle).datepicker({
			showOn: "both",
			hideIfNoPrevNext: true,
			dateFormat: "dd/mm/yy",
			defaultDate: currentDate,
			changeMonth: true,
			changeYear: true,
			buttonImageOnly: true,
			buttonText: "Open",
			buttonImage: "images/datepicker/calendar.gif",
			yearRange: '1900:2050'
		});
	}
	
	function setCalendarComponentDisabledFutureDates(calEle){
		$(calEle).datepicker({
			showOn: "both",
			hideIfNoPrevNext: true,
			dateFormat: "dd/mm/yy",
			defaultDate: currentDate,
			changeMonth: true,
			changeYear: true,
			maxDate: "today",
			buttonImageOnly: true,
			buttonText: "Open",
			buttonImage: "images/datepicker/calendar.gif",
			yearRange: '1900:2050'
		});
	}
	
	
	/*function checkKey(obj,evt,msgId) {
		var code = window.event?evt.keyCode:evt.which;	 //   0-9a-zA-Z/()-.,'\r\n 
		alert(code); 
		if ((code>47 && code<58) || (code>64 && code<91) || (code>96 && code<123) || 
				code==47 || code==40 || code==41 || code==45 || code==46 || code==39 || 
				code==44 || code==13 || code==32 || code==8 || code==127) {
		
			if (obj.value.length<2000) {
				document.getElementById(msgId).innerHTML = 1999-obj.value.length+" characters left";
				return true;
			} else {
			alert("You have reached your maximum limit");
			return false;
			}
		} else {
			alert("Character not allowed");
			return false;
		}
	}*/
	
	function substringText(obj,evt,msgId) {
		var code = window.event?evt.keyCode:evt.which;
		if(obj.value.length > 2000){
			obj.value = obj.value.substring(0,2000);
			document.getElementById(msgId).innerHTML = 2000 - obj.value.length+" characters left";
			alert(obj.value.length);
		}
		else{
			document.getElementById(msgId).innerHTML = 2000 - obj.value.length+" characters left";
		}
	}
	
	//ujjawala 04/01/2013
	var prHomeStaffRowIndex = 3;//$(".prHomeStaffs").length;
	var prHomeLocationRowIndex =  3;//$(".addressLocation").length;
	
	function removeMorePrHomeStaffEle(ele, classNameEleToModify) {
	//	alert("prHomeStaffRowIndex: "+prHomeStaffRowIndex+" prHomeLocationRowIndex: "+prHomeLocationRowIndex);
	//	alert("ele: "+ele+" classNameEleToModify: "+classNameEleToModify);
	    var parentEle = $(ele).parent().parent().parent();
		$(ele).parents("."+classNameEleToModify).remove();
		$("."+classNameEleToModify).each(function(i){
		//	alert("i"+i+"$(domEle).attr('name'): "+$(domEle).attr("name"));
			$(this).find(":input, :radio").each(function(index, domEle) {
				if($(domEle).attr("name")) { // to deal with IE' HTMLUnknownElement error
					// increment  name & id values for new rows
					var inputName = $(domEle).attr("name");
					//inputName = inputName.split("").reverse().join("").replace(/\d+/, i).split("").reverse().join("");
					
					inputName = inputName.replace(/\d+/, i);
					var inputId = $(domEle).attr("id");
					//inputId = inputId.split("").reverse().join("").replace(/_\d+_/, '_' + i + '_').split("").reverse().join("");
				//	inputId = inputId.replace(/_\d+_/, '_' + i + '_');
					inputId = inputId.replace(/\d+/, i);
					
					$(domEle).attr("name", inputName).attr("id", inputId);
					
					if($(this).attr('type') == 'radio') {
						if (window.ActiveXObject) {// Fix For IE
							var new_ele = null;
							if (domEle.checked)
								new_ele = document.createElement('<input type="radio" name="'+ inputName +'" checked="checked"/>');
							else 
								new_ele = document.createElement('<input type="radio" name="'+ inputName +'"/>');
							new_ele.id = inputId;
							new_ele.value = domEle.value;
							var parent_node = domEle.parentNode; 
							parent_node.insertBefore(new_ele, domEle);
							parent_node.removeChild(domEle);
						}
					}
				}
			});
		});
		$("."+classNameEleToModify).each(function(i){
				$(this).find("label").each(function(){
					var labelFor = $(this).attr("for");
					labelFor = labelFor.replace(/_\d_/, '_' + i + '_');
					$(this).attr("for", labelFor);
				});
		});
		
		if (classNameEleToModify=="addressLocation" || classNameEleToModify=="history" || classNameEleToModify=="exPostFactoSanction") {
			setLocationHeadingNo();
			setCalendarsForSimilarTypes("].proposedDateOfCamps");
			setCalendarsForSimilarTypes("].orderDate");
			try {
				setLoationLen();
			} catch (e) {
			}
		}
		
		if (className=="form2ContVisitOfOfficials") setCalendarsForSimilarTypes("].officersDateVisit");
		if (className=="form2ContVisitByDoctor") setCalendarsForSimilarTypes("].doctorDateVisit");
		
		if (classNameEleToModify=="grantsReceived") setCalendarsForSimilarTypes("].date");
		if (classNameEleToModify=="inmatesCopy") setCalendarsForSimilarTypes("].admissionDate");
		if (classNameEleToModify=="teachersPerticulars") setCalendarsForSimilarTypes("].dateOfJoining");
		if(classNameEleToModify=="counsellorsCopy") {
									  setCalendarsForSimilarTypes("].joiningDate");
									  setCalendarsForSimilarTypes("].relievingDate");  
									}
		setCalendarsForSimilarTypes("].sanctionDate");	
		setCalendarsForSimilarTypes("].releaseDate");	
							
	   if (classNameEleToModify=="staffDetails") setCalendarsForSimilarTypes("].appointmentDate");
	   clacAllTotalForRemove('.itemwiseCalc', '.itemwiseTotal');
	   clacAllTotalForRemove('.subTotal1', '.subTotals');
	   clacAllTotalForRemove('.subTotals','.grandTotal');
	   
	   var i = 1;
		$(parentEle).find(".insNo").each(function(){
		 $(this).val(i);
		  i++;
		});
	}
	
	/**
	 * This function is used for adding new P&R Home Location
	 * @param className
	 * @since Ujjawala 02-01-2013
	 */
	jQuery.fn.addMorePrHomeLocations = function (className){
	//	alert("addMorePrHomeLocation=======");
		if(className=='exPostFactoSanction'){
			if($("."+className).length >= 3){
			    alert("Ex-post Facto sanction details are already added for past 3 years.");
			   return false;
			   }
		}
		var cloneElement = $("." + className + ":eq(0)").clone()
									/*.find(".prHomeStaffs").find(":input, :radio, :a")
										.each(function(index, domEle){
											if($(domEle).attr("name")){
												alert("className: "+className);
												var inputName1 = $(domEle).attr("name");
												inputName1 = inputName1.replace(/[\d]/, $("." + className).length);
														
												var inputId1 = $(domEle).attr("id");
												inputId1 = inputId1.replace(/[\d]/, $("." + className).length);
														
												$(domEle).attr("name", inputName1)
																.attr("id", inputId1);
												
												if($(this).attr('type') == 'radio') {
													$(domEle).attr("checked", false);
													if (window.ActiveXObject) {// Fix For IE
														var new_ele = document.createElement('<input type="radio" name="'+ inputName1 +'"/>');
														new_ele.id = inputId1;
														new_ele.value = domEle.value;
														var parent_node = domEle.parentNode; 
														parent_node.insertBefore(new_ele, domEle);
														parent_node.removeChild(domEle);
													}
												} else if($(this).attr('type') == 'select-one') {
													this.selectedIndex = 0;
												} else {
													// reset the value
													$(this).val('');
												}
											}
											
										})
									.end()*/// go up to clone() element
									
									.find(":input, :radio, :tr ,:a") // reset the form elements of new rows
										.each(function(index, domEle) {
											
											// Increment 'name' & 'id' index for the dynamically adding elements
											if($(domEle).attr("name")) { // to deal with IE' HTMLUnknownElement error
												// increment  name values for new rows
												var inputName = $(domEle).attr("name");
										//		alert("$(\".\" + className).length: "+$("." + className).length);
												inputName = inputName.replace(/[\d]/, $("." + className).length);
												
												// increment  id values for new rows
												var inputId = $(domEle).attr("id");
												inputId = inputId.replace(/_\d_/, '_' + $("." + className).length + '_');
												
											//	alert("after replacing, inputId : "+inputId);
												
												$(domEle).attr("name", inputName)
												
														.attr("id", inputId);
												
												// if radio button, reset the checked state
												if($(this).attr('type') == 'radio') {
													$(domEle).attr("checked", false);
													if (window.ActiveXObject) {// Fix For IE
														var new_ele = document.createElement('<input type="radio" name="'+ inputName +'"/>');
														new_ele.id = inputId;
														new_ele.value = domEle.value;
														var parent_node = domEle.parentNode; 
														parent_node.insertBefore(new_ele, domEle);
														parent_node.removeChild(domEle);
													}
												} else if($(this).attr('type') == 'select-one') {
													this.selectedIndex = 0;
												} else {
													// reset the value
													$(this).val('');
												}
											}
										})
										.end() // go up to clone() element
										
									.find("label")
										.each(function(index, domEle) {
											var labelFor = $(domEle).attr("for");
											labelFor = labelFor.replace(/_\d_/, '_' + $("." + className).length + '_');
											$(domEle).attr("for", labelFor);
										})
										.end(); // go up to clone() element
									if(className == 'exPostFactoSanction')	{
										$(cloneElement).find(".pastLiability").each(function(i) {
											if (i>0) $(this).remove();
											if(i==0) $(this).find(".insNo").val("1");
										});
										var cloneElementAgain = cloneElement.insertAfter($(".exPostFactoSanction:last"));
									}else
									 	var cloneElementAgain = cloneElement.insertBefore($(this).parent().parent());
										
										cloneElementAgain.find(".locationHead").each (function (){
										var len = $("." + className).length;
										$(this).html("<b>Location"+len+" Details:</b>&nbsp;&nbsp;<a class='removeLink' href='javascript: void(0)' onclick='removeMoreEle(this, &quot;"+ className +"&quot;)' title='Click here to remove location'>Remove Location</a>");
									})
									.end()
									.find("ul.errorMessage,.AddmoreError").remove().end() // leave existing error messages;
									.find(".programmeName").attr("style","display:none;");
									
									setLocationHeadingNo();
									setCalendarsForSimilarTypes("].proposedDateOfCamps");
									setCalendarsForSimilarTypes("].orderDate");
									setCalendarsForSimilarTypes("].sanctionDate");
									setCalendarsForSimilarTypes("].releaseDate");
									
		return this; // to maintain the jQuery chaining
		
	};
	
	jQuery.fn.addMorePrHomeStaffs = function (className){
		
		$("." + className + ":eq(0)").clone()
		
		.find(":input, :radio ") // reset the form elements of new rows
			.each(function(index, domEle) {
				//alert('>>> ' + $(domEle) + " : " + $(domEle).attr("type") + " : " + domEle);

				// Increment 'name' & 'id' index for the dynamically adding elements
				if($(domEle).attr("name")) { // to deal with IE' HTMLUnknownElement error
					// increment  name & id values for new rows
					var inputName = $(domEle).attr("name");
					inputName = inputName.replace(/[\d]/, $("." + className).length);

					var inputId = $(domEle).attr("id");
				//	inputId = inputId.replace(/_\d_/, '_' + $("." + className).length + '_');
					inputId = inputId.replace(/[\d]/, $("." + className).length);
					
					$(domEle).attr("name", inputName).attr("id", inputId);
					
					// if radio button, reset the checked state
					if($(this).attr('type') == 'radio') {
						$(domEle).attr("checked", false);
						if (window.ActiveXObject) {// Fix For IE
							var new_ele = document.createElement('<input type="radio" name="'+ inputName +'"/>');
							new_ele.id = inputId;
							new_ele.value = domEle.value;
							var parent_node = domEle.parentNode; 
							parent_node.insertBefore(new_ele, domEle);
							parent_node.removeChild(domEle);
						}
					} else if($(this).attr('type') == 'select-one') {
						this.selectedIndex = 0;
					} else {
						// reset the value
						$(this).val('');
					}
				}
			})
			.end() // go up to clone() element
		.find("label")
			.each(function(index, domEle) {
				var labelFor = $(domEle).attr("for");
				labelFor = labelFor.replace(/_\d_/, '_' + $("." + className).length + '_');
				$(domEle).attr("for", labelFor);
			})
		.end() // go up to clone() element
		.find(".mandy").remove().end()
		.find("ul.errorMessage,.AddmoreError").remove().end()
		.insertBefore($(this).parent().parent()).end()
		.find("td:last").children(":last").after("&nbsp;&nbsp;<a class='removeLink' href='javascript: void(0)' onclick='removeMorePrHomeStaffEle(this, &quot;"+ className +"&quot;)' title='Click here to remove'>X</a>");
		
		 // leave existing error messages;
		/*$("."+className).each(function(y){
			$(this).find(".removeAddMore").remove();
			if (y>0) {
				$(this).find("td:last").children(":last").after("<a class='removeAddMore' style='margin-left:5px;color:red;' href='javascript: void(0)' onclick='removeMoreEle(this, &quot;"+ className +"&quot;)' title='Click here to remove'>X</a>")
			}
		});*/
		
		if (className=="form2ContVisitOfOfficials") setCalendarsForSimilarTypes("].officersDateVisit");
		if (className=="form2ContVisitByDoctor") setCalendarsForSimilarTypes("].doctorDateVisit");
		
		if (className=="grantsReceived") setCalendarsForSimilarTypes("].date");
		if (className=="inmatesCopy") setCalendarsForSimilarTypes("].admissionDate");
		if (className=="teachersPerticulars") setCalendarsForSimilarTypes("].dateOfJoining");
		if(className=="counsellorsCopy") {
		  setCalendarsForSimilarTypes("].joiningDate");
		  setCalendarsForSimilarTypes("].relievingDate");  
		}
		if (className=="staffDetails") setCalendarsForSimilarTypes("].appointmentDate");
		//$("."+className).next(".removeLink").remove();
		//$("."+className).each(function(i){if(i>0)$(this).find(".mandy").remove();});// removing mandatory asterisks
		setCalendarsForSimilarTypes("].releaseDate");
		var i = 1;
		$(this).parent().parent().parent().find(".insNo").each(function(){
		  $(this).val(i);
		  i++;
		});
		return this; // to maintain the jQuery chaining
	};
	
