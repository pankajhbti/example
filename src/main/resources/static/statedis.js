/**
 * 
 */

$(document).ready(function() {
	console.log("HI-------------");
	$('#stateId').on('change', function() {
		console.log("stateid selected is " + $('#stateId').val());
		var stateId = $('#stateId').val();
		$.ajax({
			url: 'districtOnState',
			type: 'GET',
			contentType: 'application/json; charset=utf-8',
			dataType: 'json', // request type html/json/xml //
			data: { stateId: stateId },
			success: function(data) {
				$('#comboboxDistrict').find('option').remove().end();
				$('#comboboxDistrict').append(jQuery("<option />").attr("value", '').text('-- Please select --'));
				if (data != null) {
					for (var i = 0; i < data.length; i++) {
						$('#comboboxDistrict').append('<option value="' + data[i].district_code + '">' + data[i].district_name + '</option>');
						console.log(('<option value="' + data[i].districtId + '">' + data[i].districtName + '</option>'));
					}
				}
			},
			error: function(e) {
				console.log(e.message);
			}
		});
	});
});