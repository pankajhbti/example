package com.nic.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nic.model.BackOfficeUser;
import com.nic.model.Designation;
import com.nic.model.Role;
import com.nic.repo.BackOfficeUserRepo;
import com.nic.repo.DesignationRep;
import com.nic.repo.RoleRepository;

@Component
@Transactional
public class BacKOfficeDAO {
	@Autowired
	BackOfficeUserRepo backOfficeUserRepo;
	@Autowired
	DesignationRep designationRep;
	@Autowired
	RoleRepository roleRepository;
	@Autowired
	Role role;

	
	
	
	public BackOfficeUser isActivation(Long userId)
	{
		BackOfficeUser user = backOfficeUserRepo.findById(userId).orElse(null);
		user.setIsenabled(true);
		BackOfficeUser activeUser = backOfficeUserRepo.save(user);
		return activeUser;
	}
	
	public BackOfficeUser isDeactive(Long userId)
	{
		BackOfficeUser user = backOfficeUserRepo.findById(userId).orElse(null);
		user.setIsenabled(false);
		BackOfficeUser deactiveUser = backOfficeUserRepo.save(user);
		return deactiveUser;
	}
	
	
	
	
	public BackOfficeUser saveBackOfficUser(BackOfficeUser officeUser) {

		BackOfficeUser officeUsersaved = backOfficeUserRepo.save(officeUser);
		role.setDesignation_id(officeUsersaved.getDesignationId());
		role.setUser_id(officeUsersaved.getUserId());
		roleRepository.save(role);

		return officeUsersaved;

	}

	public List<Designation> getAllDesignationListDesignation() {
		List<Designation> list = designationRep.findAll();
		return list;

	}

	public List<BackOfficeUser> getAllUser(Integer id) {
		List<BackOfficeUser> userList = backOfficeUserRepo.finaallBackOfficeUsers(id);
		return userList;
	}

	public BackOfficeUser getUserDetail(Long id) {
		BackOfficeUser user = backOfficeUserRepo.findById(id).orElse(null);

		return user;
	}

	public String getDesignation(Integer id) {
		Designation designation = designationRep.findBydesignationId(id);
		return designation.getDesignationName();

	}

}
