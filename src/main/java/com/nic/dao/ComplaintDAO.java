package com.nic.dao;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nic.dto.ComplaintDto;
import com.nic.model.DBFile;
import com.nic.model.Disability;
import com.nic.model.DisablityPercentage;
import com.nic.model.District;
import com.nic.model.OrgCategories;
import com.nic.model.State;
import com.nic.model.SubDepartment;
import com.nic.model.SubOrg;
import com.nic.repo.ComplaintRepo;
import com.nic.repo.DBFileRepository;
import com.nic.repo.DisabilityRepo;
import com.nic.repo.DisablityPerRepo;
import com.nic.repo.DistrictRepository;
import com.nic.repo.OrgCategoriseRepositry;
import com.nic.repo.StateRepository;
import com.nic.repo.SubDepartRepositry;
import com.nic.repo.SubOrgRepositry;

@Service
@Transactional

public class ComplaintDAO {
	@Autowired
	ComplaintRepo complaintRepo;
	@Autowired
	ComplaintDto complaintDto;
	@Autowired
	StateRepository stateRepository;
	@Autowired
	DistrictRepository districtRepository;
	@Autowired
	DisablityPerRepo disablityPerRepo;
	@Autowired
	OrgCategoriseRepositry orgCategoriseRepositry;
	@Autowired
	SubDepartRepositry subDepartRepositry;
	@Autowired
	Disability disability;
	@Autowired
	SubOrgRepositry subOrgRepositry;
	@Autowired
	DisabilityRepo disabilityRepo;
	@Autowired
	private DBFileRepository dbFileRepository;

	public Long complaintSaveAsDraft(ComplaintDto complaintDto) {

		complaintDto = complaintRepo.save(complaintDto);

		Long refno = complaintDto.getRefno();
		return refno;
	}

	public ComplaintDto finalSubmit(Long refno) {
		complaintDto = complaintRepo.findById(refno).orElse(null);

		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		String strDate = formatter.format(date);

		complaintDto.setComplaintStatus("for Processing at O/o CCPD");

		complaintDto.setFinalSubmit(strDate);

		return complaintRepo.save(complaintDto);

	}

	public List<ComplaintDto> findByCompEmail(String email) {

		List<ComplaintDto> complaintDto = complaintRepo.findBycEmail(email);

		return complaintDto;

	}

	public List<ComplaintDto> findByReperEmail(String email) {
		List<ComplaintDto> complaintDto = complaintRepo.findByrEmail(email);

		return complaintDto;

	}

	public List<ComplaintDto> findByOffice() {
		List<ComplaintDto> complaintDto = complaintRepo.findBycomplainantType("o");

		return complaintDto;

	}

	public ComplaintDto findByRefNo(Long refno) {
		complaintDto = complaintRepo.findById(refno).orElse(null);

		return complaintDto;
	}

	public String stateName(Integer code)

	{
		State state = stateRepository.findBystatecodeid(code);

		return state.getStateName();
	}

	public String distName(Integer code) {
		District dist = districtRepository.findBydistrictCode(code);
		return dist.getDistrictName();
	}

	public String percentage(Integer code)

	{
		DisablityPercentage percentage = disablityPerRepo.findBypercentageCode(code);

		return percentage.getDisabilityValue();
	}

	public String orgName(Integer code) {
		OrgCategories orgName = orgCategoriseRepositry.findByOrgCode(code);

		return orgName.getOrgName();

	}

	public String subOrgtName(Integer code) {
		SubOrg subOrg = subOrgRepositry.findBySubOrgCode(code);

		return subOrg.getSubOrgName();

	}

	public String department(Integer code) {
		SubDepartment department = subDepartRepositry.findByDeptCode(code);

		return department.getDeptName();

	}

	public String disbalityCategoriseName(Integer code) {
		Disability disability = disabilityRepo.findByDisabilityId(code);
		return disability.getDisabilityType();

	}

	public List<DBFile> listOfFileByUser(Long refno) {
		List<DBFile> dbFileTemp = dbFileRepository.findByReferenceNo(refno);
		return dbFileTemp;
	}

	public List<State> stateData() {
		List<State> statelist = stateRepository.findAll();
		return statelist;
	}

	public List<District> distData() {
		List<District> distlist = districtRepository.findAll();
		return distlist;
	}

	public List<OrgCategories> orgData() {
		List<OrgCategories> orgList = orgCategoriseRepositry.findAll();
		return orgList;
	}

	public List<Disability> disablityData() {
		List<Disability> dlist = disabilityRepo.findAll();
		return dlist;
	}

	public List<DisablityPercentage> disablityPercentage() {
		List<DisablityPercentage> perList = disablityPerRepo.findAll();
		return perList;
	}

	public void designation() {

	}

}
