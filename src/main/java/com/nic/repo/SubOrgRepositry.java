package com.nic.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nic.model.SubOrg;

public interface SubOrgRepositry extends JpaRepository<SubOrg, Integer>{

	    List<SubOrg>findByOrgcode(Integer orgcode);
	    
	    SubOrg  findBySubOrgCode(Integer subOrgCode);
	    
}
