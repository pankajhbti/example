package com.nic.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nic.dto.ComplaintDto;

public interface ComplaintRepo extends JpaRepository<ComplaintDto, Long> {

	List<ComplaintDto> findBycEmail(String email);

	
	List<ComplaintDto>  findByrEmail(String rEmail);
	
	
	List<ComplaintDto> findBycomplainantType(String complainantType);
	
	
	
}
