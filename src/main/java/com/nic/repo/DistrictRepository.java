package com.nic.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nic.model.District;

@Repository
public interface DistrictRepository extends JpaRepository<District, Integer> {

	List<District> findByStatecodeid(Integer statecodeid);
	
	District findBydistrictCode(Integer districtCode);
}
