package com.nic.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.nic.model.BackOfficeUser;

public interface BackOfficeUserRepo extends JpaRepository<BackOfficeUser, Long> {
	BackOfficeUser findByemail(String email);
	
	 @Query("select u from BackOfficeUser u where u.designationId > ?1 ")
	   List<BackOfficeUser> finaallBackOfficeUsers(Integer id);
	
	
}
