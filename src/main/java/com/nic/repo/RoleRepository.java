package com.nic.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.nic.model.Role;
@EnableJpaRepositories
public interface RoleRepository extends JpaRepository<Role, Integer> {

}
