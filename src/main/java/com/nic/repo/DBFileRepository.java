package com.nic.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.nic.model.DBFile;

@Repository
public interface DBFileRepository extends JpaRepository<DBFile, Long> {
	/* DBFile findByFileDownloadUri(String fileDownloadUri); */

	DBFile findByFileDownloadUri(String fileDownloadUri);

	@Query("SELECT dbf FROM DBFile dbf WHERE dbf.referenceNo = :refNo and dbf.docsType=:docsType")
	DBFile findByRefeAndDocDesc(Long refNo, String docsType);

	@Query("SELECT dbf FROM DBFile dbf WHERE dbf.referenceNo = :refNo and dbf.docsType=:docsType")
	List<DBFile> findByRefeAndDocDescList(Long refNo, String docsType);
	
	@Query("SELECT dbf FROM DBFile dbf WHERE dbf.referenceNo = :refNo and dbf.docsType=:docsType and dbf.serialNo=:serialNo")

    DBFile findByRefeAndDocDescForMulti(Long refNo,String docsType,Long serialNo);

	@Query("SELECT dbf FROM DBFile dbf WHERE dbf.id = :id")
	DBFile findByFilePathById(Long id);

	DBFile findTopByOrderByIdDesc();

	List<DBFile> findByReferenceNo(Long referenceNo);
}