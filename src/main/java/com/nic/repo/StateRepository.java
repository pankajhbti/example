package com.nic.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nic.model.State;

@Repository
public interface StateRepository extends JpaRepository<State, Integer> {

	State findBystatecodeid(Integer statecodeid);
}
