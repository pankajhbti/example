package com.nic.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nic.model.OrgCategories;

@Repository
public interface OrgCategoriseRepositry extends JpaRepository<OrgCategories, Integer>{

	OrgCategories findByOrgCode(Integer orgCode);
	
	
	
}
