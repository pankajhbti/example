package com.nic.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.nic.model.Designation;
@Component
public interface DesignationRep extends JpaRepository<Designation, Long>{

	Designation findBydesignationId(Integer designationId);

	
}
