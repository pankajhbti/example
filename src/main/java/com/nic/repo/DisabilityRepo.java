package com.nic.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nic.model.Disability;

@Repository
public interface DisabilityRepo extends JpaRepository<Disability, Integer> {

	Disability  findByDisabilityId(Integer disabilityId);

}
