package com.nic.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.nic.model.User;

@Repository("userRepository")
public interface UserRepository extends JpaRepository<User, Long> {

	 User findByemailIgnoreCase(String email);

	 @Query("SELECT u FROM User u WHERE u.email = ?1")
	User findByEmail(String email);
	 
	 public User findByResetPasswordToken(String token);
	
}
