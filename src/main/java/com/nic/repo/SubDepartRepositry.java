package com.nic.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nic.model.SubDepartment;

public interface SubDepartRepositry extends JpaRepository<SubDepartment, Integer>{

	List<SubDepartment>findBysubOrgCode(Integer subOrgCode);
	SubDepartment  findByDeptCode(Integer deptCode);
	
}
