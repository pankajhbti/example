package com.nic.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nic.model.DisablityPercentage;

public interface DisablityPerRepo extends JpaRepository<DisablityPercentage, Integer> {
	DisablityPercentage findBypercentageCode(Integer percentageCode);
}
