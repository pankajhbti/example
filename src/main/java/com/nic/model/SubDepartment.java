package com.nic.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "master_sub_department")
public class SubDepartment {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "dept_code")
	private Integer deptCode;
	
	@Column(name = "sub_org_code")
	private Integer subOrgCode;
	
	@Column(name = "dept_name")
	private String deptName;

	public SubDepartment() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getDeptCode() {
		return deptCode;
	}

	public void setDeptCode(Integer deptCode) {
		this.deptCode = deptCode;
	}

	public Integer getSubOrgCode() {
		return subOrgCode;
	}

	public void setSubOrgCode(Integer subOrgCode) {
		this.subOrgCode = subOrgCode;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	@Override
	public String toString() {
		return "SubDepartment [deptCode=" + deptCode + ", subOrgCode=" + subOrgCode + ", deptName=" + deptName + "]";
	}
	
	

}
