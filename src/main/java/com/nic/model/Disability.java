package com.nic.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;
@Component
@Entity
@Table(name = "master_disability")
public class Disability {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "disability_id")
	private Integer disabilityId;

	@Column(name = "disability_type")
	private String disabilityType;

	public Disability() {
		super();
	
	}

	public Integer getDisabilityId() {
		return disabilityId;
	}

	public void setDisabilityId(Integer disabilityId) {
		this.disabilityId = disabilityId;
	}

	public String getDisabilityType() {
		return disabilityType;
	}

	public void setDisabilityType(String disabilityType) {
		this.disabilityType = disabilityType;
	}

	
	
	
}
