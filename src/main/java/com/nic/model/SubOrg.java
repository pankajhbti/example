package com.nic.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "master_sub_organization")
public class SubOrg {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sub_org_code")
	private Integer subOrgCode;
	
	@Column(name = "sub_org_name")
	private String subOrgName;
	
	@Column(name = "org_code")
	private Integer orgcode;

	public SubOrg() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getSubOrgCode() {
		return subOrgCode;
	}

	public void setSubOrgCode(Integer subOrgCode) {
		this.subOrgCode = subOrgCode;
	}

	public String getSubOrgName() {
		return subOrgName;
	}

	public void setSubOrgName(String subOrgName) {
		this.subOrgName = subOrgName;
	}

	public Integer getOrgcode() {
		return orgcode;
	}

	public void setOrgcode(Integer orgcode) {
		this.orgcode = orgcode;
	}

	@Override
	public String toString() {
		return "SubOrg [subOrgCode=" + subOrgCode + ", subOrgName=" + subOrgName + ", orgcode=" + orgcode + "]";
	}

	
	
	

}
