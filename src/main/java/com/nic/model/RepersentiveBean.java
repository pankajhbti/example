package com.nic.model;

import org.springframework.stereotype.Component;

@Component
public class RepersentiveBean {

	/* user detatils */
	private String userName;
	private String fatherHusbandName;
	private String email;
	private Integer statecodeid;
	private Integer districtCode;
	private String gender;
	private String addressOne;
	private String addressTwo;
	private String postoffice;
	private String pin;
	private String userMobilePhone;
	
	
	
	/* complantaint deatials */

	private String cName;
	private String cFatherHusbandName;
	private String cGender;
	private Integer cState;
	private Integer cDistrict;
	private String cAddress1;
	private String cAddress2;
	private String cPostOffice;
	private String cPin;
	private String cEmail;
	private String cMobile;
	private Integer cAge;
	/*-------------------------*/

	private String complainantType;

	private Integer disabilityType;
	private Integer disabilityPercentage;

	private String certificateIssuingName;
	private String certificateIssueDate;
	private Integer certificateIssueState;
	private Integer certificateIssueDistrict;
	/*
	 * private String complaintDate; private String receiptDate;
	 */
	private String othersOrg;
	private Integer orgCode;

	private Integer subOrgCode;
	private Integer departmentCode;
	private String headOfOrg;
	private Integer stateOfOrg;
	private Integer districtOfOrg;
	private String addressOfOrg1;
	private String addressOfOrg2;
	private String postofficeOfOrg;
	private String pinCodeOfOrg;
	private String emailOfOrg;
	private String mobileOfOrg;
	private String phoneOfOrg;

	private String incidenceDate;
	private String incidencePlace;
	private String grievanceDescription;
	private String reliefInSought;
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getFatherHusbandName() {
		return fatherHusbandName;
	}
	public void setFatherHusbandName(String fatherHusbandName) {
		this.fatherHusbandName = fatherHusbandName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Integer getStatecodeid() {
		return statecodeid;
	}
	public void setStatecodeid(Integer statecodeid) {
		this.statecodeid = statecodeid;
	}
	public Integer getDistrictCode() {
		return districtCode;
	}
	public void setDistrictCode(Integer districtCode) {
		this.districtCode = districtCode;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getAddressOne() {
		return addressOne;
	}
	public void setAddressOne(String addressOne) {
		this.addressOne = addressOne;
	}
	public String getAddressTwo() {
		return addressTwo;
	}
	public void setAddressTwo(String addressTwo) {
		this.addressTwo = addressTwo;
	}
	public String getPostoffice() {
		return postoffice;
	}
	public void setPostoffice(String postoffice) {
		this.postoffice = postoffice;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public String getUserMobilePhone() {
		return userMobilePhone;
	}
	public void setUserMobilePhone(String userMobilePhone) {
		this.userMobilePhone = userMobilePhone;
	}
	
	public Integer getcAge() {
		return cAge;
	}
	public void setcAge(Integer cAge) {
		this.cAge = cAge;
	}
	public String getcName() {
		return cName;
	}
	public void setcName(String cName) {
		this.cName = cName;
	}
	public String getcFatherHusbandName() {
		return cFatherHusbandName;
	}
	public void setcFatherHusbandName(String cFatherHusbandName) {
		this.cFatherHusbandName = cFatherHusbandName;
	}
	public String getcGender() {
		return cGender;
	}
	public void setcGender(String cGender) {
		this.cGender = cGender;
	}
	public Integer getcState() {
		return cState;
	}
	public void setcState(Integer cState) {
		this.cState = cState;
	}
	public Integer getcDistrict() {
		return cDistrict;
	}
	public void setcDistrict(Integer cDistrict) {
		this.cDistrict = cDistrict;
	}
	public String getcAddress1() {
		return cAddress1;
	}
	public void setcAddress1(String cAddress1) {
		this.cAddress1 = cAddress1;
	}
	public String getcAddress2() {
		return cAddress2;
	}
	public void setcAddress2(String cAddress2) {
		this.cAddress2 = cAddress2;
	}
	public String getcPostOffice() {
		return cPostOffice;
	}
	public void setcPostOffice(String cPostOffice) {
		this.cPostOffice = cPostOffice;
	}
	public String getcPin() {
		return cPin;
	}
	public void setcPin(String cPin) {
		this.cPin = cPin;
	}
	public String getcEmail() {
		return cEmail;
	}
	public void setcEmail(String cEmail) {
		this.cEmail = cEmail;
	}
	public String getcMobile() {
		return cMobile;
	}
	public void setcMobile(String cMobile) {
		this.cMobile = cMobile;
	}
	public String getComplainantType() {
		return complainantType;
	}
	public void setComplainantType(String complainantType) {
		this.complainantType = complainantType;
	}
	public Integer getDisabilityType() {
		return disabilityType;
	}
	public void setDisabilityType(Integer disabilityType) {
		this.disabilityType = disabilityType;
	}
	public Integer getDisabilityPercentage() {
		return disabilityPercentage;
	}
	public void setDisabilityPercentage(Integer disabilityPercentage) {
		this.disabilityPercentage = disabilityPercentage;
	}
	public String getCertificateIssuingName() {
		return certificateIssuingName;
	}
	public void setCertificateIssuingName(String certificateIssuingName) {
		this.certificateIssuingName = certificateIssuingName;
	}
	public String getCertificateIssueDate() {
		return certificateIssueDate;
	}
	public void setCertificateIssueDate(String certificateIssueDate) {
		this.certificateIssueDate = certificateIssueDate;
	}
	public Integer getCertificateIssueState() {
		return certificateIssueState;
	}
	public void setCertificateIssueState(Integer certificateIssueState) {
		this.certificateIssueState = certificateIssueState;
	}
	public Integer getCertificateIssueDistrict() {
		return certificateIssueDistrict;
	}
	public void setCertificateIssueDistrict(Integer certificateIssueDistrict) {
		this.certificateIssueDistrict = certificateIssueDistrict;
	}

	/*
	 * public String getComplaintDate() { return complaintDate; } public void
	 * setComplaintDate(String complaintDate) { this.complaintDate = complaintDate;
	 * } public String getReceiptDate() { return receiptDate; } public void
	 * setReceiptDate(String receiptDate) { this.receiptDate = receiptDate; }
	 */
	public String getOthersOrg() {
		return othersOrg;
	}
	public void setOthersOrg(String othersOrg) {
		this.othersOrg = othersOrg;
	}
	public Integer getOrgCode() {
		return orgCode;
	}
	public void setOrgCode(Integer orgCode) {
		this.orgCode = orgCode;
	}
	public Integer getSubOrgCode() {
		return subOrgCode;
	}
	public void setSubOrgCode(Integer subOrgCode) {
		this.subOrgCode = subOrgCode;
	}
	public Integer getDepartmentCode() {
		return departmentCode;
	}
	public void setDepartmentCode(Integer departmentCode) {
		this.departmentCode = departmentCode;
	}
	public String getHeadOfOrg() {
		return headOfOrg;
	}
	public void setHeadOfOrg(String headOfOrg) {
		this.headOfOrg = headOfOrg;
	}
	public Integer getStateOfOrg() {
		return stateOfOrg;
	}
	public void setStateOfOrg(Integer stateOfOrg) {
		this.stateOfOrg = stateOfOrg;
	}
	public Integer getDistrictOfOrg() {
		return districtOfOrg;
	}
	public void setDistrictOfOrg(Integer districtOfOrg) {
		this.districtOfOrg = districtOfOrg;
	}
	public String getAddressOfOrg1() {
		return addressOfOrg1;
	}
	public void setAddressOfOrg1(String addressOfOrg1) {
		this.addressOfOrg1 = addressOfOrg1;
	}
	public String getAddressOfOrg2() {
		return addressOfOrg2;
	}
	public void setAddressOfOrg2(String addressOfOrg2) {
		this.addressOfOrg2 = addressOfOrg2;
	}
	public String getPostofficeOfOrg() {
		return postofficeOfOrg;
	}
	public void setPostofficeOfOrg(String postofficeOfOrg) {
		this.postofficeOfOrg = postofficeOfOrg;
	}
	public String getPinCodeOfOrg() {
		return pinCodeOfOrg;
	}
	public void setPinCodeOfOrg(String pinCodeOfOrg) {
		this.pinCodeOfOrg = pinCodeOfOrg;
	}
	public String getEmailOfOrg() {
		return emailOfOrg;
	}
	public void setEmailOfOrg(String emailOfOrg) {
		this.emailOfOrg = emailOfOrg;
	}
	public String getMobileOfOrg() {
		return mobileOfOrg;
	}
	public void setMobileOfOrg(String mobileOfOrg) {
		this.mobileOfOrg = mobileOfOrg;
	}
	public String getIncidenceDate() {
		return incidenceDate;
	}
	public void setIncidenceDate(String incidenceDate) {
		this.incidenceDate = incidenceDate;
	}
	public String getIncidencePlace() {
		return incidencePlace;
	}
	public void setIncidencePlace(String incidencePlace) {
		this.incidencePlace = incidencePlace;
	}
	public String getGrievanceDescription() {
		return grievanceDescription;
	}
	public void setGrievanceDescription(String grievanceDescription) {
		this.grievanceDescription = grievanceDescription;
	}
	public String getReliefInSought() {
		return reliefInSought;
	}
	public void setReliefInSought(String reliefInSought) {
		this.reliefInSought = reliefInSought;
	}
	public String getPhoneOfOrg() {
		return phoneOfOrg;
	}
	public void setPhoneOfOrg(String phoneOfOrg) {
		this.phoneOfOrg = phoneOfOrg;
	}
	
	

}
