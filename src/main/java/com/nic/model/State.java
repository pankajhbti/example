package com.nic.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "master_state")
public class State {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "statecodeid")
	private Integer statecodeid;
	
	@Column(name = "state_version")
	private Integer stateVersion;
	
	@Column(name = "state_name")
	private String stateName;
	
	@Column(name = "state_NL_name")
	private String stateNLName;
	
	@Column(name = "state_census_code_2001")
	private Integer statecensuscode2001;
	
	@Column(name = "state_census_code_2011")
	private Integer statecensuscode2011;
	
	@Column(name = "state_ut")
	private String stateUt;

	public State() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getStatecodeid() {
		return statecodeid;
	}

	public void setStatecodeid(Integer statecodeid) {
		this.statecodeid = statecodeid;
	}

	public Integer getStateVersion() {
		return stateVersion;
	}

	public void setStateVersion(Integer stateVersion) {
		this.stateVersion = stateVersion;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getStateNLName() {
		return stateNLName;
	}

	public void setStateNLName(String stateNLName) {
		this.stateNLName = stateNLName;
	}

	public Integer getStatecensuscode2001() {
		return statecensuscode2001;
	}

	public void setStatecensuscode2001(Integer statecensuscode2001) {
		this.statecensuscode2001 = statecensuscode2001;
	}

	public Integer getStatecensuscode2011() {
		return statecensuscode2011;
	}

	public void setStatecensuscode2011(Integer statecensuscode2011) {
		this.statecensuscode2011 = statecensuscode2011;
	}

	public String getStateUt() {
		return stateUt;
	}

	public void setStateUt(String stateUt) {
		this.stateUt = stateUt;
	}

	
}
