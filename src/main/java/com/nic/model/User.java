package com.nic.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Table(name = "user_registration_details")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "user_id")
	private long userId;
	
	@Column(name = "email")
	@NotNull
	@Pattern(regexp = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", message = "Email address is invalid")
	private String email;

	@Size(max = 10, min = 10, message = "Mobile number should be of 10 digits")
	@Pattern(regexp = "(^$|[0-9]{10})")
	@Column(name = "user_mobile_phone")
	private String userMobilePhone;
	
	@Column(length = 50)
	private String gender;
	// manish 29/12
	@Size(max = 10)
	@NotNull
	@Column(name = "complainant_type")
	private String complainantType;

	/* private String complaint_representative; */
	@Column(name = "address_one")
	private String addressOne;

	@Column(name = "address_two")
	private String addressTwo;

	@Column(name = "postoffice")
	private String postoffice;

	@Column(length = 6)
	private String pin;

	private String password;

	@Column(name = "father_husband_name")
	private String fatherHusbandName;

	private Integer age;

	@Column(length = 10)
	private Integer statecodeid;
	
	@Column(name = "district_code",length = 10)
	private Integer districtCode;
	
	@NotNull
	@Column(name = "username")
	private String userName;

	
	@Column(name = "disability_type",length = 10)
	private Integer disabilityType;

	@Column(name = "disability_percentage",length = 10)
	private Integer disabilityPercentage;

	private boolean isenabled;
	@Column(name = "reset_password_token")

	private String resetPasswordToken;
	@Transient
	private Set<Role> roles;

	public User() {
		super();

	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserMobilePhone() {
		return userMobilePhone;
	}

	public void setUserMobilePhone(String userMobilePhone) {
		this.userMobilePhone = userMobilePhone;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getComplainantType() {
		return complainantType;
	}

	public void setComplainantType(String complainantType) {
		this.complainantType = complainantType;
	}

	public String getAddressOne() {
		return addressOne;
	}

	public void setAddressOne(String addressOne) {
		this.addressOne = addressOne;
	}

	public String getAddressTwo() {
		return addressTwo;
	}

	public void setAddressTwo(String addressTwo) {
		this.addressTwo = addressTwo;
	}

	public String getPostoffice() {
		return postoffice;
	}

	public void setPostoffice(String postoffice) {
		this.postoffice = postoffice;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFatherHusbandName() {
		return fatherHusbandName;
	}

	public void setFatherHusbandName(String fatherHusbandName) {
		this.fatherHusbandName = fatherHusbandName;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Integer getStatecodeid() {
		return statecodeid;
	}

	public void setStatecodeid(Integer statecodeid) {
		this.statecodeid = statecodeid;
	}

	public Integer getDistrictCode() {
		return districtCode;
	}

	public void setDistrictCode(Integer districtCode) {
		this.districtCode = districtCode;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getDisabilityType() {
		return disabilityType;
	}

	public void setDisabilityType(Integer disabilityType) {
		this.disabilityType = disabilityType;
	}

	public Integer getDisabilityPercentage() {
		return disabilityPercentage;
	}

	public void setDisabilityPercentage(Integer disabilityPercentage) {
		this.disabilityPercentage = disabilityPercentage;
	}

	public boolean isIsenabled() {
		return isenabled;
	}

	public void setIsenabled(boolean isenabled) {
		this.isenabled = isenabled;
	}

	public String getResetPasswordToken() {
		return resetPasswordToken;
	}

	public void setResetPasswordToken(String resetPasswordToken) {
		this.resetPasswordToken = resetPasswordToken;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

}
