package com.nic.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "master_district")
public class District {
	@Column(name = "statecodeid")
	private Integer statecodeid;
	
	@Column(name = "state_name")
	private String stateName;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "district_code")
	private Integer districtCode;
	
	@Column(name = "district_version")
	private String districtVersion;
	
	@Column(name = "district_name")
	private String districtName;
	
	@Column(name = "district_NL_name")
	private String districtNLname;

	public District() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getStatecodeid() {
		return statecodeid;
	}

	public void setStatecodeid(Integer statecodeid) {
		this.statecodeid = statecodeid;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public Integer getDistrictCode() {
		return districtCode;
	}

	public void setDistrictCode(Integer districtCode) {
		this.districtCode = districtCode;
	}

	public String getDistrictVersion() {
		return districtVersion;
	}

	public void setDistrictVersion(String districtVersion) {
		this.districtVersion = districtVersion;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public String getDistrictNLname() {
		return districtNLname;
	}

	public void setDistrictNLname(String districtNLname) {
		this.districtNLname = districtNLname;
	}

	@Override
	public String toString() {
		return "District [statecodeid=" + statecodeid + ", stateName=" + stateName + ", districtCode=" + districtCode
				+ ", districtVersion=" + districtVersion + ", districtName=" + districtName + ", districtNLname="
				+ districtNLname + "]";
	}

	
	
	
	
	
	
	
}
