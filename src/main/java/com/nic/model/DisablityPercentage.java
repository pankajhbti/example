package com.nic.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;
@Component
@Entity
@Table(name = "master_disability_percentage")
public class DisablityPercentage {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "percentage_code")
	private Integer percentageCode;
	
	@Column(name = "percentage_value")
	private String disabilityValue;
	
	
	
	
	

	public DisablityPercentage() {
		super();
		
	}

	public Integer getPercentageCode() {
		return percentageCode;
	}

	public void setPercentageCode(Integer percentageCode) {
		this.percentageCode = percentageCode;
	}

	public String getDisabilityValue() {
		return disabilityValue;
	}

	public void setDisabilityValue(String disabilityValue) {
		this.disabilityValue = disabilityValue;
	}

	
}
