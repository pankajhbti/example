package com.nic.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "back_office_registration")
public class BackOfficeUser {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "user_id")
	private Long userId;

	/* @NotNull */
	@Column(name = "user_name")
	private String userName;

	@Column(name = "email")
	/*
	 * @NotNull
	 * 
	 * @Pattern(regexp =
	 * "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$",
	 * message = "Email address is invalid")
	 */
	private String email;

	@Column(name = "mobile")

	private String mobile;

	@Column(length = 50)
	private String gender;

	@Column(length = 50)
	private Integer age;

	@Column(length = 50)
	private Integer designationId;

	
	private boolean isenabled;

	@Column(name = "user_password")
	private String userPassword;

	@Transient
	private String designationName;
	
	private Date userDateTime;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)

	@JoinTable(name = "user_role", 
	joinColumns = @JoinColumn(name = "user_id"), 
	inverseJoinColumns = @JoinColumn(name = "designation_id"))
	List<Designation> roles;

	/*
	 * @ManyToMany(targetEntity = Designation.class,mappedBy = ("Role"), cascade =
	 * CascadeType.ALL) private List<Designation> roles;
	 */

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}



	public boolean isIsenabled() {
		return isenabled;
	}

	public void setIsenabled(boolean isenabled) {
		this.isenabled = isenabled;
	}

	public Date getUserDateTime() {
		return userDateTime;
	}

	public void setUserDateTime(Date userDateTime) {
		this.userDateTime = userDateTime;
	}

	public Integer getDesignationId() {
		return designationId;
	}

	public void setDesignationId(Integer designationId) {
		this.designationId = designationId;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public List<Designation> getRoles() {
		return roles;
	}

	public void setRoles(List<Designation> roles) {
		this.roles = roles;
	}

	public String getDesignationName() {
		return designationName;
	}

	public void setDesignationName(String designationName) {
		this.designationName = designationName;
	}

	
	
	
	
	
	
	
	

}
