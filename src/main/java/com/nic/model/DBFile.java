package com.nic.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "uploaded_files")
public class DBFile {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "file_type")
	private String fileType;

	@Column(name = "file_name")
	private String fileName;

	@Column(name = "document_desc")
	private String documentDesc;

	@Column(name = "file_exten")
	private String fileExten;

	@Column(name = "ref_no")
	private Long referenceNo;

	@Column(name = "path")
	private String fileDownloadUri;

	@Column(name = "time_stamp")
	private Date timeStamp;

	@Column(name = "serial_no")

	private Long serialNo;
	
	@Column(name = "docs_type")
	private String docsType;

	public DBFile() {

	}

	public DBFile(Long id) {

		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getFileDownloadUri() {
		return fileDownloadUri;
	}

	public void setFileDownloadUri(String fileDownloadUri) {
		this.fileDownloadUri = fileDownloadUri;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileExten() {
		return fileExten;
	}

	public void setFileExten(String fileExten) {
		this.fileExten = fileExten;
	}

	

	public Long getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(Long referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getDocumentDesc() {
		return documentDesc;
	}

	public void setDocumentDesc(String documentDesc) {
		this.documentDesc = documentDesc;
	}

	public Date getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	public Long getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(Long serialNo) {
		this.serialNo = serialNo;
	}

	public String getDocsType() {
		return docsType;
	}

	public void setDocsType(String docsType) {
		this.docsType = docsType;
	}


	
	

}
