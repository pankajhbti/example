package com.nic.model;

import org.springframework.stereotype.Component;

@Component
public class ComplaintPdfBean {
	private Long refno;
	private String fileNo;
	private String complaintStatus;
	private String requestMode;
	private String cName;
	private String cFatherHusbandName;
	private String cState;
	private String cDistrict;
	private String cAddress1;
	private String cAddress2;
	private String cPin;
	private String cPostOffice;
	private String cEmail;
	private String cMobile;
	private String cGender;
	private Integer age;
	private String disabilityType;
	private String disabilityPer;
	private String certificateIssuingName;
	private String certificateIssueDate;
	private String certificateIssueState;
	private String certificateIssueDistrict;
	private String receiptDate;
	private String rName;
	private String rFatherHusbandName;
	private String rState;
	private String rDistrict;
	private String rAddress1;
	private String rAddress2;
	private String rPostOffice;
	private String rPin;
	private String rEmail;
	private String rMobile;
	private String rGender;
	private String orgCode;
	private String othersOrg;
	private String subOrgCode;
	private String departmentCode;
	private String headOfOrg;
	private String stateOfOrg;
	private String districtOfOrg;
	private String addressOfOrg1;
	private String addressOfOrg2;
	private String postofficeOfOrg;
	private String pinCodeOfOrg;
	private String emailOfOrg;
	private String mobileOfOrg;
	private String phoneOfOrg;
	private String incidenceDate;
	private String incidencePlace;
	private String grievanceDescription;
	private String reliefInSought;
	private String documentPath; 
	private String finalSubmit;
	private String complaintDate;

	public String getDisabilityPer() {
		return disabilityPer;
	}
	public void setDisabilityPer(String disabilityPer) {
		this.disabilityPer = disabilityPer;
	}
	
	
	public Long getRefno() {
		return refno;
	}
	public void setRefno(Long refno) {
		this.refno = refno;
	}
	public String getFileNo() {
		return fileNo;
	}
	public void setFileNo(String fileNo) {
		this.fileNo = fileNo;
	}
	public String getComplaintStatus() {
		return complaintStatus;
	}
	public void setComplaintStatus(String complaintStatus) {
		this.complaintStatus = complaintStatus;
	}
	public String getRequestMode() {
		return requestMode;
	}
	public void setRequestMode(String requestMode) {
		this.requestMode = requestMode;
	}
	public String getcName() {
		return cName;
	}
	public void setcName(String cName) {
		this.cName = cName;
	}
	public String getcFatherHusbandName() {
		return cFatherHusbandName;
	}
	public void setcFatherHusbandName(String cFatherHusbandName) {
		this.cFatherHusbandName = cFatherHusbandName;
	}
	public String getcState() {
		return cState;
	}
	public void setcState(String cState) {
		this.cState = cState;
	}
	public String getcDistrict() {
		return cDistrict;
	}
	public void setcDistrict(String cDistrict) {
		this.cDistrict = cDistrict;
	}
	public String getcAddress1() {
		return cAddress1;
	}
	public void setcAddress1(String cAddress1) {
		this.cAddress1 = cAddress1;
	}
	public String getcAddress2() {
		return cAddress2;
	}
	public void setcAddress2(String cAddress2) {
		this.cAddress2 = cAddress2;
	}
	public String getcPin() {
		return cPin;
	}
	public void setcPin(String cPin) {
		this.cPin = cPin;
	}
	public String getcPostOffice() {
		return cPostOffice;
	}
	public void setcPostOffice(String cPostOffice) {
		this.cPostOffice = cPostOffice;
	}
	public String getcEmail() {
		return cEmail;
	}
	public void setcEmail(String cEmail) {
		this.cEmail = cEmail;
	}
	public String getcMobile() {
		return cMobile;
	}
	public void setcMobile(String cMobile) {
		this.cMobile = cMobile;
	}
	public String getcGender() {
		return cGender;
	}
	public void setcGender(String cGender) {
		this.cGender = cGender;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public String getDisabilityType() {
		return disabilityType;
	}
	public void setDisabilityType(String disabilityType) {
		this.disabilityType = disabilityType;
	}
	public String getCertificateIssuingName() {
		return certificateIssuingName;
	}
	public void setCertificateIssuingName(String certificateIssuingName) {
		this.certificateIssuingName = certificateIssuingName;
	}
	public String getCertificateIssueDate() {
		return certificateIssueDate;
	}
	public void setCertificateIssueDate(String certificateIssueDate) {
		this.certificateIssueDate = certificateIssueDate;
	}
	public String getCertificateIssueState() {
		return certificateIssueState;
	}
	public void setCertificateIssueState(String certificateIssueState) {
		this.certificateIssueState = certificateIssueState;
	}
	public String getCertificateIssueDistrict() {
		return certificateIssueDistrict;
	}
	public void setCertificateIssueDistrict(String certificateIssueDistrict) {
		this.certificateIssueDistrict = certificateIssueDistrict;
	}
	public String getReceiptDate() {
		return receiptDate;
	}
	public void setReceiptDate(String receiptDate) {
		this.receiptDate = receiptDate;
	}
	public String getrName() {
		return rName;
	}
	public void setrName(String rName) {
		this.rName = rName;
	}
	public String getrFatherHusbandName() {
		return rFatherHusbandName;
	}
	public void setrFatherHusbandName(String rFatherHusbandName) {
		this.rFatherHusbandName = rFatherHusbandName;
	}
	public String getrState() {
		return rState;
	}
	public void setrState(String rState) {
		this.rState = rState;
	}
	public String getrDistrict() {
		return rDistrict;
	}
	public void setrDistrict(String rDistrict) {
		this.rDistrict = rDistrict;
	}
	public String getrAddress1() {
		return rAddress1;
	}
	public void setrAddress1(String rAddress1) {
		this.rAddress1 = rAddress1;
	}
	public String getrAddress2() {
		return rAddress2;
	}
	public void setrAddress2(String rAddress2) {
		this.rAddress2 = rAddress2;
	}
	public String getrPostOffice() {
		return rPostOffice;
	}
	public void setrPostOffice(String rPostOffice) {
		this.rPostOffice = rPostOffice;
	}
	public String getrPin() {
		return rPin;
	}
	public void setrPin(String rPin) {
		this.rPin = rPin;
	}
	public String getrEmail() {
		return rEmail;
	}
	public void setrEmail(String rEmail) {
		this.rEmail = rEmail;
	}
	public String getrMobile() {
		return rMobile;
	}
	public void setrMobile(String rMobile) {
		this.rMobile = rMobile;
	}
	public String getrGender() {
		return rGender;
	}
	public void setrGender(String rGender) {
		this.rGender = rGender;
	}
	public String getOrgCode() {
		return orgCode;
	}
	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}
	public String getOthersOrg() {
		return othersOrg;
	}
	public void setOthersOrg(String othersOrg) {
		this.othersOrg = othersOrg;
	}
	public String getSubOrgCode() {
		return subOrgCode;
	}
	public void setSubOrgCode(String subOrgCode) {
		this.subOrgCode = subOrgCode;
	}
	public String getDepartmentCode() {
		return departmentCode;
	}
	public void setDepartmentCode(String departmentCode) {
		this.departmentCode = departmentCode;
	}
	public String getHeadOfOrg() {
		return headOfOrg;
	}
	public void setHeadOfOrg(String headOfOrg) {
		this.headOfOrg = headOfOrg;
	}
	public String getStateOfOrg() {
		return stateOfOrg;
	}
	public void setStateOfOrg(String stateOfOrg) {
		this.stateOfOrg = stateOfOrg;
	}
	public String getDistrictOfOrg() {
		return districtOfOrg;
	}
	public void setDistrictOfOrg(String districtOfOrg) {
		this.districtOfOrg = districtOfOrg;
	}
	public String getAddressOfOrg1() {
		return addressOfOrg1;
	}
	public void setAddressOfOrg1(String addressOfOrg1) {
		this.addressOfOrg1 = addressOfOrg1;
	}
	public String getAddressOfOrg2() {
		return addressOfOrg2;
	}
	public void setAddressOfOrg2(String addressOfOrg2) {
		this.addressOfOrg2 = addressOfOrg2;
	}
	public String getPostofficeOfOrg() {
		return postofficeOfOrg;
	}
	public void setPostofficeOfOrg(String postofficeOfOrg) {
		this.postofficeOfOrg = postofficeOfOrg;
	}
	public String getPinCodeOfOrg() {
		return pinCodeOfOrg;
	}
	public void setPinCodeOfOrg(String pinCodeOfOrg) {
		this.pinCodeOfOrg = pinCodeOfOrg;
	}
	public String getEmailOfOrg() {
		return emailOfOrg;
	}
	public void setEmailOfOrg(String emailOfOrg) {
		this.emailOfOrg = emailOfOrg;
	}
	public String getMobileOfOrg() {
		return mobileOfOrg;
	}
	public void setMobileOfOrg(String mobileOfOrg) {
		this.mobileOfOrg = mobileOfOrg;
	}
	public String getPhoneOfOrg() {
		return phoneOfOrg;
	}
	public void setPhoneOfOrg(String phoneOfOrg) {
		this.phoneOfOrg = phoneOfOrg;
	}
	public String getIncidenceDate() {
		return incidenceDate;
	}
	public void setIncidenceDate(String incidenceDate) {
		this.incidenceDate = incidenceDate;
	}
	public String getIncidencePlace() {
		return incidencePlace;
	}
	public void setIncidencePlace(String incidencePlace) {
		this.incidencePlace = incidencePlace;
	}
	public String getGrievanceDescription() {
		return grievanceDescription;
	}
	public void setGrievanceDescription(String grievanceDescription) {
		this.grievanceDescription = grievanceDescription;
	}
	public String getReliefInSought() {
		return reliefInSought;
	}
	public void setReliefInSought(String reliefInSought) {
		this.reliefInSought = reliefInSought;
	}
	public String getDocumentPath() {
		return documentPath;
	}
	public void setDocumentPath(String documentPath) {
		this.documentPath = documentPath;
	}
	public String getFinalSubmit() {
		return finalSubmit;
	}
	public void setFinalSubmit(String finalSubmit) {
		this.finalSubmit = finalSubmit;
	}
	public String getComplaintDate() {
		return complaintDate;
	}
	public void setComplaintDate(String complaintDate) {
		this.complaintDate = complaintDate;
	}
	
	
	
}
