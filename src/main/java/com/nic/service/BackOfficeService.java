package com.nic.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.nic.dao.BacKOfficeDAO;
import com.nic.model.BackOfficeUser;
import com.nic.repo.BackOfficeUserRepo;

@Service
public class BackOfficeService {
	@Autowired
	BackOfficeUserRepo backOfficeUserRepo;
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	@Autowired
	BacKOfficeDAO bacKOfficeDAO;

	public BackOfficeUser isEnable(Long userId) {
		BackOfficeUser activeUser = bacKOfficeDAO.isActivation(userId);

		return activeUser;
	}

	public BackOfficeUser isDeactive(Long userId) {

		BackOfficeUser deactiveUser = bacKOfficeDAO.isDeactive(userId);
		return deactiveUser;

	}

	public BackOfficeUser checkByEmail(String email) {

		BackOfficeUser user = backOfficeUserRepo.findByemail(email);
		return user;
	}

	public BackOfficeUser saveProcessOffice(BackOfficeUser backOfficeUser) {
		backOfficeUser.setUserPassword(passwordEncoder.encode(backOfficeUser.getUserPassword()));
		backOfficeUser.setUserDateTime(new Date());

		BackOfficeUser saveUser = bacKOfficeDAO.saveBackOfficUser(backOfficeUser);

		return saveUser;
	}

	public List<BackOfficeUser> findAllUser() {
		Integer id = 1;
		List<BackOfficeUser> userList = bacKOfficeDAO.getAllUser(id);
		return userList;

	}

	public BackOfficeUser getUserDetails(Long userId) {
		BackOfficeUser user = bacKOfficeDAO.getUserDetail(userId);
		String designationName = bacKOfficeDAO.getDesignation(user.getDesignationId());
		user.setDesignationName(designationName);
		return user;

	}

}
