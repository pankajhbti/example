package com.nic.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.util.StringUtils;
import com.nic.exception.MyFileNotFoundException;
import com.nic.model.DBFile;
import com.nic.repo.DBFileRepository;

@Service
public class DBFileStorageService {

	@Autowired
	private DBFileRepository dbFileRepository;

	public DBFile storeFile(MultipartFile file,String filepath,String documentType,String fileNames) {
		//String fileName = StringUtils.cleanPath(file.getOriginalFilename());
		System.out.println("file" + file);
		System.out.println(documentType);
		System.out.println("filename" + fileNames);
		//DBFile dbFile = new DBFile(fileName,file.getContentType());
		DBFile dbFile = new DBFile();
		dbFile.setTimeStamp(new Date());
		return dbFileRepository.save(dbFile);
		/* 
		 * String fileName = StringUtils.cleanPath(file.getOriginalFilename());
		 * 
		 * try { // Check if the file's name contains invalid characters if
		 * (fileName.contains("..")) { throw new
		 * FileStorageException("Sorry! Filename contains invalid path sequence " +
		 * fileName); }
		 * 
		 * } catch (Exception ex) { throw new
		 * FileStorageException("Could not store file " + fileName +
		 * ". Please try again!", ex); }
		 */
	}

	public DBFile getFile(Long fileId) {
		System.out.println(fileId);
		return dbFileRepository.findById(fileId)
				.orElseThrow(() -> new MyFileNotFoundException("File not found with id " + fileId));
	}
	
}
