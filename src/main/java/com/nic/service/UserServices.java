package com.nic.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.nic.exception.UserNotFoundException;
import com.nic.model.ConfirmationToken;
import com.nic.model.User;
import com.nic.repo.ConfirmationTokenRepository;
import com.nic.repo.UserRepository;

@Service
@Transactional
public class UserServices {

	@Autowired
	private ConfirmationTokenRepository confirmationTokenRepository;
	@Autowired
	UserRepository userRepository;
	
	public User checkUserByEmail(String email)
	{
		User existingUser = userRepository.findByemailIgnoreCase(email);
		return existingUser;
		
	}
	
	
	public void saveProcessUser(User user)
	{
		userRepository.save(user);
	}
	
	
	public void newUserToken(ConfirmationToken confirmationToken)
	{
		confirmationTokenRepository.save(confirmationToken);
		
	}
	
	
	public String confirmUserAccount(String confirmationToken)
	{
		ConfirmationToken token = confirmationTokenRepository.findByConfirmationToken(confirmationToken);
		if (token != null) {
			User user = userRepository.findByemailIgnoreCase(token.getUser().getEmail());
			user.setIsenabled(true);
			userRepository.save(user); 
			
			return"accountVerified";
		}
		return "The link is invalid or broken!";
		
			
	}
	
	public void updateResetPasswordToken(String token, String email) throws UserNotFoundException {
		User user = userRepository.findByEmail(email);
		if (user != null) {
			user.setResetPasswordToken(token);
			userRepository.save(user);

		} else {
			throw new UserNotFoundException("Could not find any customer with the email " + email);
		}

	}

	public User getByResetPasswordToken(String token) {
		return userRepository.findByResetPasswordToken(token);
	}
	
	public void updatePassword(User user,String newPassword)
	{
		BCryptPasswordEncoder passwordEncoder= new BCryptPasswordEncoder();
		String encodedPassword = passwordEncoder.encode(newPassword);
		user.setPassword(encodedPassword);
		user.setResetPasswordToken(null);
		userRepository.save(user);
	}
	
	
	
	
	
}
