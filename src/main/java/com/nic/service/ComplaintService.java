package com.nic.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nic.dao.ComplaintDAO;
import com.nic.dto.ComplaintDto;
import com.nic.model.ComplainantBean;
import com.nic.model.Disability;
import com.nic.model.RepersentiveBean;
import com.nic.model.User;
import com.nic.repo.UserRepository;

@Service
public class ComplaintService {

	@Autowired
	Disability disability;

	@Autowired
	ComplaintDto complaintDto;

	@Autowired
	ComplainantBean complainantBean;

	@Autowired
	ComplaintDAO complaintDAO;

	@Autowired
	RepersentiveBean repersentiveBean;

	@Autowired
	private UserRepository userRepository;
	
	/*
	 * @Autowired DisablityPerRepo disablityPerRepo;
	 */
	public User currentUserForLogin(String user)

	{
		User userCurrent = userRepository.findByEmail(user);

		return userCurrent;

	}

	public ComplainantBean copyComplainant(User user) {

		BeanUtils.copyProperties(user, complainantBean);

		return complainantBean;

	}

	public RepersentiveBean copyRepersentive(User user) {

		BeanUtils.copyProperties(user, repersentiveBean);
		return repersentiveBean;

	}

	public ComplaintDto complaintentSaveProcess(ComplainantBean complaintBean) {

		BeanUtils.copyProperties(complaintBean, complaintDto);

		complaintDto.setcName(complaintBean.getUserName());
		complaintDto.setcFatherHusbandName(complaintBean.getFatherHusbandName());
		complaintDto.setcState(complaintBean.getStatecodeid());
		complaintDto.setcDistrict(complaintBean.getDistrictCode());
		complaintDto.setcAddress1(complaintBean.getAddressOne());
		complaintDto.setcAddress2(complaintBean.getAddressTwo());
		complaintDto.setcPostOffice(complaintBean.getPostoffice());
		complaintDto.setcPin(complaintBean.getPin());
		complaintDto.setcEmail(complaintBean.getEmail());
		complaintDto.setcGender(complaintBean.getGender());
		complaintDto.setRequestMode("online");
		complaintDto.setComplaintStatus("Upload document is pending");
		complaintDto.setcMobile(complaintBean.getUserMobilePhone());

		return complaintDto;

	}

	public ComplaintDto repersentiveSaveProcess(RepersentiveBean repersentiveBean) {

		BeanUtils.copyProperties(repersentiveBean, complaintDto);
		complaintDto.setrName(repersentiveBean.getUserName());
		complaintDto.setrDistrict(repersentiveBean.getDistrictCode());
		complaintDto.setrState(repersentiveBean.getStatecodeid());
		complaintDto.setrFatherHusbandName(repersentiveBean.getFatherHusbandName());
		complaintDto.setrAddress1(repersentiveBean.getAddressOne());
		complaintDto.setrAddress2(repersentiveBean.getAddressTwo());
		complaintDto.setrPostOffice(repersentiveBean.getPostoffice());
		complaintDto.setrGender(repersentiveBean.getGender());
		complaintDto.setrPin(repersentiveBean.getPin());
		complaintDto.setrEmail(repersentiveBean.getEmail());
		complaintDto.setAge(repersentiveBean.getcAge());
		complaintDto.setRequestMode("Online");
		complaintDto.setrMobile(repersentiveBean.getUserMobilePhone());
		complaintDto.setComplaintStatus("Upload document is pending");

		return complaintDto;

	}

	/*
	 * public List<State> stateData() { List<State> statelist =
	 * stateRepository.findAll(); return statelist; }
	 * 
	 * public List<District> distData() { List<District> distlist =
	 * districtRepository.findAll(); return distlist; }
	 * 
	 * public List<OrgCategories> orgData() { List<OrgCategories> orgList =
	 * orgCategoriseRepositry.findAll(); return orgList; }
	 * 
	 * public List<Disability> disablityData() { List<Disability> dlist =
	 * disabilityRepo.findAll(); return dlist; }
	 * 
	 * public List<DisablityPercentage> disablityPercentage() {
	 * List<DisablityPercentage> perList = disablityPerRepo.findAll(); return
	 * perList; }
	 */
}
