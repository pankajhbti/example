package com.nic.service;

import org.springframework.stereotype.Component;

import com.nic.utility.SequenceGenerator;

@Component
public class SyncSequenceGenerator implements SequenceGenerator {

	long value=1;
	@Override
	public long getNext() {
		// TODO Auto-generated method stub
		return value++;
	}

}
