package com.nic.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nic.dao.ComplaintDAO;
import com.nic.dto.ComplaintDto;
import com.nic.model.ComplaintPdfBean;

@Service
public class PdfService {

	@Autowired
	ComplaintPdfBean complaintPdfBean;
	@Autowired
	ComplaintDAO complaintDAO;

	public ComplaintPdfBean getComplaintInfo(ComplaintDto dto)

	{

		if (dto.getcState() != null && !dto.getcState().equals("") && dto.getcState() > 0) {
			String cstate = complaintDAO.stateName(dto.getcState());
			String cdist = complaintDAO.distName(dto.getcDistrict());
			complaintPdfBean.setcState(cstate);
			complaintPdfBean.setcDistrict(cdist);
		}

		if (dto.getrState() != null && !dto.getrState().equals("") && dto.getrState() > 0) {
			String rstate = complaintDAO.stateName(dto.getrState());

			complaintPdfBean.setrState(rstate);
			System.out.println(dto.getrDistrict());
			String rdist = complaintDAO.distName(dto.getrDistrict());
			complaintPdfBean.setrDistrict(rdist);

			System.out.println(rdist);
		}

		/*
		 * if (dto.getrDistrict() != null && dto.getrDistrict().equals("") &&
		 * dto.getrDistrict() > 0) { System.out.println(dto.getrDistrict() + " " +
		 * "Rdistit");
		 * 
		 * }
		 */

		if (dto.getStateOfOrg() != null && !dto.getStateOfOrg().equals("") && dto.getStateOfOrg() > 0) {
			String orgstate = complaintDAO.stateName(dto.getStateOfOrg());
			String orgdist = complaintDAO.distName(dto.getDistrictOfOrg());
			complaintPdfBean.setStateOfOrg(orgstate);
			complaintPdfBean.setDistrictOfOrg(orgdist);
		}

		if (dto.getCertificateIssueState() != null) {
			String certifcatestate = complaintDAO.stateName(dto.getCertificateIssueState());
			String certifcatedist = complaintDAO.distName(dto.getCertificateIssueDistrict());
			complaintPdfBean.setCertificateIssueState(certifcatestate);
			complaintPdfBean.setCertificateIssueDistrict(certifcatedist);
		}

		if (dto.getDisabilityPer() > 0) {
			String per = complaintDAO.percentage(dto.getDisabilityPer());
			complaintPdfBean.setDisabilityPer(per);
		}

		if (dto.getOrgCode() != null && !dto.getOrgCode().equals("") && dto.getOrgCode() > 0) {
			String orgName = complaintDAO.orgName(dto.getOrgCode());
			complaintPdfBean.setOrgCode(orgName);
		}

		if (dto.getSubOrgCode() != null && !dto.getSubOrgCode().equals("") && dto.getSubOrgCode() > 0) {
			String subOrgName = complaintDAO.subOrgtName(dto.getSubOrgCode());
			complaintPdfBean.setSubOrgCode(subOrgName);
		}

		if (dto.getDepartmentCode() != null && !dto.getDepartmentCode().equals("") && dto.getDepartmentCode() > 0) {
			String departName = complaintDAO.department(dto.getDepartmentCode());
			complaintPdfBean.setDepartmentCode(departName);
		}

		if (dto.getDisabilityType() > 0) {
			String disabilityType = complaintDAO.disbalityCategoriseName(dto.getDisabilityType());
			complaintPdfBean.setDisabilityType(disabilityType);
		}

		BeanUtils.copyProperties(dto, complaintPdfBean);

		return complaintPdfBean;

	}

}
