package com.nic.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nic.model.State;
import com.nic.repo.StateRepository;

@Service
public class StateService {
	@Autowired
	private StateRepository repo;

	public List<State> getAllState() {
		return repo.findAll();
	}
}
