package com.nic.utility;

public interface SequenceGenerator {
	long getNext();
}
