package com.nic.utility;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

@Component
public class Utility {
	public  String getSiteURL(HttpServletRequest request)
	{
		String sitURL = request.getRequestURL().toString();
		
		return sitURL.replace(request.getServletPath(), "");
		
	}

}
