package com.nic.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "complaint_details")
public class ComplaintDto {

	/* complaint type info */

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ref_no")
	private Long refno;

	@Column(name = "file_no")
	private String fileNo;

	@Column(name = "complaint_status")
	private String complaintStatus;

	@Column(name = "complainant_type")
	private String complainantType;

	@Column(name = "request_mode")
	private String requestMode;

	/* complaintent deatails */

	@Column(name = "c_Name")
	private String cName;

	@Column(name = "c_Father_Husband_Name")
	private String cFatherHusbandName;

	

	@Column(name = "c_State", length = 50)
	private Integer cState;

	@Column(name = "c_District", length = 50)
	private Integer cDistrict;

	@Column(name = "c_Address1")
	private String cAddress1;

	@Column(name = "c_Address2")
	private String cAddress2;

	@Column(name = "c_PostOffice")
	private String cPostOffice;

	@Column(name = "c_Pin", length = 10)
	private String cPin;

	@Column(name = "c_Email")
	private String cEmail;

	@Column(name = "c_Mobile", length = 50)
	private String cMobile;

	@Column(name = "c_Gender", length = 50)
	private String cGender;

	@Column(name = "c_age", length = 50)
	private Integer age;

	/* disability aurthentication details */

	@Column(name = "disability_type", length = 10)
	private Integer disabilityType;

	@Column(name = "disability_percentage", length = 50)
	private Integer disabilityPer;

	@Column(name = "certificate_issuing_name")
	private String certificateIssuingName;

	@Column(name = "certificate_issue_Date", length = 50)
	private String certificateIssueDate;

	@Column(name = "certificate_issue_state", length = 50)
	private Integer certificateIssueState;

	@Column(name = "certificate_issue_District", length = 50)
	private Integer certificateIssueDistrict;

	/* Details of Complaint */
	@Column(name = "complaint_Date", length = 50)
	private String complaintDate;

	@Column(name = "receipte_Date", length = 50)
	private String receiptDate;

	/* detail of repersentive */

	@Column(name = "r_name")
	private String rName;

	@Column(name = "r_father_husband_name")
	private String rFatherHusbandName;

	@Column(name = "r_state", length = 50)
	private Integer rState;

	@Column(name = "r_district", length = 50)
	private Integer rDistrict;

	@Column(name = "r_address")
	private String rAddress1;

	@Column(name = "r_address2")
	private String rAddress2;

	@Column(name = "r_postOffice")
	private String rPostOffice;

	@Column(name = "r_pin", length = 50)
	private String rPin;

	@Column(name = "r_email")
	private String rEmail;

	@Column(name = "r_mobile", length = 50)
	private String rMobile;

	@Column(name = "r_gender", length = 50)
	private String rGender;

	/* repondent */

	@Column(name = "org_categories_code", length = 50)
	private Integer orgCode;

	@Column(name = "others_org")
	private String othersOrg;

	@Column(name = "sub_org_code", length = 50)
	private Integer subOrgCode;

	@Column(name = "department_code", length = 50)
	private Integer departmentCode;

	@Column(name = "head_of_org")
	private String headOfOrg;

	@Column(name = "state_of_org", length = 50)
	private Integer stateOfOrg;

	@Column(name = "district_of_org", length = 50)
	private Integer districtOfOrg;

	@Column(name = "address_of_org1")
	private String addressOfOrg1;

	@Column(name = "address_of_org2")
	private String addressOfOrg2;

	@Column(name = "postoffice_of_org")
	private String postofficeOfOrg;

	@Column(name = "pinCode_of_org", length = 50)
	private String pinCodeOfOrg;

	@Column(name = "email_of_org", length = 250)
	private String emailOfOrg;

	@Column(name = "mobile_of_org", length = 50)
	private String mobileOfOrg;
	@Column(name = "phone_of_org", length = 50)
	private String phoneOfOrg;

	/* Details of Complaint brief */

	@Column(name = "incidence_Date", length = 50)
	private String incidenceDate;

	@Column(name = "incidence_Place")
	private String incidencePlace;

	@Column(name = "grievance_description", length = 6000)
	private String grievanceDescription;

	@Column(name = "relief_in_sought", length = 6000)
	private String reliefInSought;

	

	
	@Column(name = "finalSubmit_date")
	 private String finalSubmit;
	/* getter and stter */

	public ComplaintDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	


	public Long getRefno() {
		return refno;
	}



	public void setRefno(Long refno) {
		this.refno = refno;
	}



	public String getFileNo() {
		return fileNo;
	}

	public void setFileNo(String fileNo) {
		this.fileNo = fileNo;
	}

	public String getComplaintStatus() {
		return complaintStatus;
	}

	public void setComplaintStatus(String complaintStatus) {
		this.complaintStatus = complaintStatus;
	}

	public String getComplainantType() {
		return complainantType;
	}

	public void setComplainantType(String complainantType) {
		this.complainantType = complainantType;
	}

	public String getRequestMode() {
		return requestMode;
	}

	public void setRequestMode(String requestMode) {
		this.requestMode = requestMode;
	}

	public String getcName() {
		return cName;
	}

	public void setcName(String cName) {
		this.cName = cName;
	}

	public String getcFatherHusbandName() {
		return cFatherHusbandName;
	}

	public void setcFatherHusbandName(String cFatherHusbandName) {
		this.cFatherHusbandName = cFatherHusbandName;
	}

	public Integer getcState() {
		return cState;
	}

	public void setcState(Integer cState) {
		this.cState = cState;
	}

	public Integer getcDistrict() {
		return cDistrict;
	}

	public void setcDistrict(Integer cDistrict) {
		this.cDistrict = cDistrict;
	}

	public String getcAddress1() {
		return cAddress1;
	}

	public void setcAddress1(String cAddress1) {
		this.cAddress1 = cAddress1;
	}

	public String getcAddress2() {
		return cAddress2;
	}

	public void setcAddress2(String cAddress2) {
		this.cAddress2 = cAddress2;
	}

	public String getcPostOffice() {
		return cPostOffice;
	}

	public void setcPostOffice(String cPostOffice) {
		this.cPostOffice = cPostOffice;
	}

	public String getcPin() {
		return cPin;
	}

	public void setcPin(String cPin) {
		this.cPin = cPin;
	}

	public String getcEmail() {
		return cEmail;
	}

	public void setcEmail(String cEmail) {
		this.cEmail = cEmail;
	}

	public String getcMobile() {
		return cMobile;
	}

	public void setcMobile(String cMobile) {
		this.cMobile = cMobile;
	}

	public String getcGender() {
		return cGender;
	}

	public void setcGender(String cGender) {
		this.cGender = cGender;
	}

	public Integer getDisabilityType() {
		return disabilityType;
	}

	public void setDisabilityType(Integer disabilityType) {
		this.disabilityType = disabilityType;
	}



	public Integer getDisabilityPer() {
		return disabilityPer;
	}




	public void setDisabilityPer(Integer disabilityPer) {
		this.disabilityPer = disabilityPer;
	}




	public String getCertificateIssuingName() {
		return certificateIssuingName;
	}

	public void setCertificateIssuingName(String certificateIssuingName) {
		this.certificateIssuingName = certificateIssuingName;
	}

	public String getCertificateIssueDate() {
		return certificateIssueDate;
	}

	public void setCertificateIssueDate(String certificateIssueDate) {
		this.certificateIssueDate = certificateIssueDate;
	}

	public Integer getCertificateIssueState() {
		return certificateIssueState;
	}

	public void setCertificateIssueState(Integer certificateIssueState) {
		this.certificateIssueState = certificateIssueState;
	}

	public Integer getCertificateIssueDistrict() {
		return certificateIssueDistrict;
	}

	public void setCertificateIssueDistrict(Integer certificateIssueDistrict) {
		this.certificateIssueDistrict = certificateIssueDistrict;
	}

	public String getComplaintDate() {
		return complaintDate;
	}

	public void setComplaintDate(String complaintDate) {
		this.complaintDate = complaintDate;
	}

	public String getReceiptDate() {
		return receiptDate;
	}

	public void setReceiptDate(String receiptDate) {
		this.receiptDate = receiptDate;
	}

	public String getrName() {
		return rName;
	}

	public void setrName(String rName) {
		this.rName = rName;
	}

	public String getrFatherHusbandName() {
		return rFatherHusbandName;
	}

	public void setrFatherHusbandName(String rFatherHusbandName) {
		this.rFatherHusbandName = rFatherHusbandName;
	}

	public Integer getrState() {
		return rState;
	}

	public void setrState(Integer rState) {
		this.rState = rState;
	}

	public Integer getrDistrict() {
		return rDistrict;
	}

	public void setrDistrict(Integer rDistrict) {
		this.rDistrict = rDistrict;
	}

	public String getrAddress1() {
		return rAddress1;
	}

	public void setrAddress1(String rAddress1) {
		this.rAddress1 = rAddress1;
	}

	public String getrAddress2() {
		return rAddress2;
	}

	public void setrAddress2(String rAddress2) {
		this.rAddress2 = rAddress2;
	}

	public String getrPostOffice() {
		return rPostOffice;
	}

	public void setrPostOffice(String rPostOffice) {
		this.rPostOffice = rPostOffice;
	}

	public String getrPin() {
		return rPin;
	}

	public void setrPin(String rPin) {
		this.rPin = rPin;
	}

	public String getrEmail() {
		return rEmail;
	}

	public void setrEmail(String rEmail) {
		this.rEmail = rEmail;
	}

	public String getrMobile() {
		return rMobile;
	}

	public void setrMobile(String rMobile) {
		this.rMobile = rMobile;
	}

	public String getrGender() {
		return rGender;
	}

	public void setrGender(String rGender) {
		this.rGender = rGender;
	}

	public Integer getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(Integer orgCode) {
		this.orgCode = orgCode;
	}

	public String getOthersOrg() {
		return othersOrg;
	}

	public void setOthersOrg(String othersOrg) {
		this.othersOrg = othersOrg;
	}

	public Integer getSubOrgCode() {
		return subOrgCode;
	}

	public void setSubOrgCode(Integer subOrgCode) {
		this.subOrgCode = subOrgCode;
	}

	public Integer getDepartmentCode() {
		return departmentCode;
	}

	public void setDepartmentCode(Integer departmentCode) {
		this.departmentCode = departmentCode;
	}

	public String getHeadOfOrg() {
		return headOfOrg;
	}

	public void setHeadOfOrg(String headOfOrg) {
		this.headOfOrg = headOfOrg;
	}

	public Integer getStateOfOrg() {
		return stateOfOrg;
	}

	public void setStateOfOrg(Integer stateOfOrg) {
		this.stateOfOrg = stateOfOrg;
	}

	public Integer getDistrictOfOrg() {
		return districtOfOrg;
	}

	public void setDistrictOfOrg(Integer districtOfOrg) {
		this.districtOfOrg = districtOfOrg;
	}

	public String getPostofficeOfOrg() {
		return postofficeOfOrg;
	}

	public void setPostofficeOfOrg(String postofficeOfOrg) {
		this.postofficeOfOrg = postofficeOfOrg;
	}

	public String getPinCodeOfOrg() {
		return pinCodeOfOrg;
	}

	public void setPinCodeOfOrg(String pinCodeOfOrg) {
		this.pinCodeOfOrg = pinCodeOfOrg;
	}

	public String getEmailOfOrg() {
		return emailOfOrg;
	}

	public void setEmailOfOrg(String emailOfOrg) {
		this.emailOfOrg = emailOfOrg;
	}

	public String getMobileOfOrg() {
		return mobileOfOrg;
	}

	public void setMobileOfOrg(String mobileOfOrg) {
		this.mobileOfOrg = mobileOfOrg;
	}

	public String getIncidenceDate() {
		return incidenceDate;
	}

	public void setIncidenceDate(String incidenceDate) {
		this.incidenceDate = incidenceDate;
	}

	public String getIncidencePlace() {
		return incidencePlace;
	}

	public void setIncidencePlace(String incidencePlace) {
		this.incidencePlace = incidencePlace;
	}

	public String getGrievanceDescription() {
		return grievanceDescription;
	}

	public void setGrievanceDescription(String grievanceDescription) {
		this.grievanceDescription = grievanceDescription;
	}

	public String getReliefInSought() {
		return reliefInSought;
	}

	public void setReliefInSought(String reliefInSought) {
		this.reliefInSought = reliefInSought;
	}


	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getAddressOfOrg1() {
		return addressOfOrg1;
	}

	public void setAddressOfOrg1(String addressOfOrg1) {
		this.addressOfOrg1 = addressOfOrg1;
	}

	public String getAddressOfOrg2() {
		return addressOfOrg2;
	}

	public void setAddressOfOrg2(String addressOfOrg2) {
		this.addressOfOrg2 = addressOfOrg2;
	}

	public String getPhoneOfOrg() {
		return phoneOfOrg;
	}

	public void setPhoneOfOrg(String phoneOfOrg) {
		this.phoneOfOrg = phoneOfOrg;
	}

	
	
	
	public String getFinalSubmit() {
		return finalSubmit;
	}

	public void setFinalSubmit(String finalSubmit) {
		this.finalSubmit = finalSubmit;
	}




	@Override
	public String toString() {
		return "ComplaintDto [refno=" + refno + ", fileNo=" + fileNo + ", complaintStatus=" + complaintStatus
				+ ", complainantType=" + complainantType + ", requestMode=" + requestMode + ", cName=" + cName
				+ ", cFatherHusbandName=" + cFatherHusbandName + ", cState=" + cState + ", cDistrict=" + cDistrict
				+ ", cAddress1=" + cAddress1 + ", cAddress2=" + cAddress2 + ", cPostOffice=" + cPostOffice + ", cPin="
				+ cPin + ", cEmail=" + cEmail + ", cMobile=" + cMobile + ", cGender=" + cGender + ", age=" + age
				+ ", disabilityType=" + disabilityType + ", disabilityPer=" + disabilityPer
				+ ", certificateIssuingName=" + certificateIssuingName + ", certificateIssueDate="
				+ certificateIssueDate + ", certificateIssueState=" + certificateIssueState
				+ ", certificateIssueDistrict=" + certificateIssueDistrict + ", complaintDate=" + complaintDate
				+ ", receiptDate=" + receiptDate + ", rName=" + rName + ", rFatherHusbandName=" + rFatherHusbandName
				+ ", rState=" + rState + ", rDistrict=" + rDistrict + ", rAddress1=" + rAddress1 + ", rAddress2="
				+ rAddress2 + ", rPostOffice=" + rPostOffice + ", rPin=" + rPin + ", rEmail=" + rEmail + ", rMobile="
				+ rMobile + ", rGender=" + rGender + ", orgCode=" + orgCode + ", othersOrg=" + othersOrg
				+ ", subOrgCode=" + subOrgCode + ", departmentCode=" + departmentCode + ", headOfOrg=" + headOfOrg
				+ ", stateOfOrg=" + stateOfOrg + ", districtOfOrg=" + districtOfOrg + ", addressOfOrg1=" + addressOfOrg1
				+ ", addressOfOrg2=" + addressOfOrg2 + ", postofficeOfOrg=" + postofficeOfOrg + ", pinCodeOfOrg="
				+ pinCodeOfOrg + ", emailOfOrg=" + emailOfOrg + ", mobileOfOrg=" + mobileOfOrg + ", phoneOfOrg="
				+ phoneOfOrg + ", incidenceDate=" + incidenceDate + ", incidencePlace=" + incidencePlace
				+ ", grievanceDescription=" + grievanceDescription + ", reliefInSought=" + reliefInSought
				+ ", finalSubmit=" + finalSubmit + "]";
	}






	
	
	
}
