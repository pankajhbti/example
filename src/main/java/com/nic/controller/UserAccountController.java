package com.nic.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.nic.dao.ComplaintDAO;
import com.nic.dto.ComplaintDto;
import com.nic.model.ComplainantBean;
import com.nic.model.ConfirmationToken;
import com.nic.model.DBFile;
import com.nic.model.Disability;
import com.nic.model.DisablityPercentage;
import com.nic.model.District;
import com.nic.model.OrgCategories;
import com.nic.model.RepersentiveBean;
import com.nic.model.State;
import com.nic.model.User;
import com.nic.repo.ComplaintRepo;
import com.nic.repo.DBFileRepository;
import com.nic.repo.SubDepartRepositry;
import com.nic.repo.SubOrgRepositry;
import com.nic.service.ComplaintService;
import com.nic.service.DBFileStorageService;
import com.nic.service.EmailSenderService;
import com.nic.service.UserServices;

@Controller
public class UserAccountController {
	private static final Logger logger = LoggerFactory.getLogger(UserAccountController.class);

	@Autowired
	private EmailSenderService emailService;

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Autowired
	SubOrgRepositry subOrgRepositry;

	@Autowired
	SubDepartRepositry subDepartRepositry;

	@Autowired
	ComplaintService complaintService;

	@Autowired
	ComplaintDAO complaintDAO;

	@Autowired
	ComplainantBean complainantBean;

	@Autowired
	ComplaintDto complaintDto;

	@Autowired
	RepersentiveBean repersentiveBean;
	@Autowired
	UserServices userServices;

	@Autowired
	ComplaintRepo complaintRepo;

	@Autowired
	private DBFileRepository dbFileRepository;

	// manish

	@GetMapping({ "/", "/deshBorad" })
	public String viewHomePage(Authentication authentication, Model model) {

		User userCurrent = complaintService.currentUserForLogin(authentication.getName());

		if (userCurrent.getComplainantType().equalsIgnoreCase("c")) {

			List<ComplaintDto> complaintDto = complaintDAO.findByCompEmail(userCurrent.getEmail());
			model.addAttribute("user", userCurrent);
			model.addAttribute("dto", complaintDto);

			/* provide view page acording to complaint register on not */

			if (complaintDto.size() > 0) {
				return "user-DashBoard";
			}

			return "new-UserDashBoard";
		}

		else if (userCurrent.getComplainantType().equalsIgnoreCase("o"))

		{

			List<ComplaintDto> complaintDto = complaintDAO.findByOffice();
			model.addAttribute("user", userCurrent);
			model.addAttribute("dto", complaintDto);
			/* provide view page acording to complaint register on not */
			if (complaintDto.size() > 0) {
				return "user-DashBoard";
			}
			
			 return "new-UserDashBoard"; 
		}

		else {
			userCurrent.getEmail();
			List<ComplaintDto> complaintDto = complaintDAO.findByReperEmail(userCurrent.getEmail());
			model.addAttribute("user", userCurrent);
			model.addAttribute("dto", complaintDto);
			/* provide view page acording to complaint register on not */
			if (complaintDto.size() > 0) {
				return "user-DashBoard";
			}

			return "new-UserDashBoard";
		}

	}

	@RequestMapping("/login")
	public String login() {

		return "login";

	}

	@RequestMapping("/logout-success")
	public String logoutPage() {
		return "login";
	}

	/* after final submit after upload doc */

	@GetMapping("finalSubmit/{refno}")
	public String finalSubmit(@PathVariable("refno") Long refno, Model model) {

		List<DBFile> dbFileTemp = dbFileRepository.findByReferenceNo(refno);

		for (DBFile list : dbFileTemp) {
			if (list.getDocumentDesc().equalsIgnoreCase("Disability Certificate")) {

				ComplaintDto dto = complaintDAO.finalSubmit(refno);

				return "redirect:/deshBorad";
			}

		}

		model.addAttribute("msg", "Please Upload Document");
		return "document";
	}

	@GetMapping("welcome")
	public String welcome() {

		return "welcome";
	}

	@RequestMapping(value = "/grivances", method = RequestMethod.GET)

	@ResponseBody

	public ModelAndView currentUser(Authentication authentication, ModelAndView mv) {

		/* String user = authentication.getName(); */

		User userCurrent = complaintService.currentUserForLogin(authentication.getName());

		List<DisablityPercentage> perList = complaintDAO.disablityPercentage();
		mv.addObject("perList", perList);
		List<State> statelist = complaintDAO.stateData();
		mv.addObject("stateList", statelist);

		List<District> distlist = complaintDAO.distData();
		mv.addObject("distlist", distlist);

		List<OrgCategories> orgList = complaintDAO.orgData();
		mv.addObject("orgList", orgList);

		List<Disability> dlist = complaintDAO.disablityData();
		mv.addObject("disability", dlist);

		/* view grivancepage acording to user */

		if (userCurrent.getComplainantType().equalsIgnoreCase("c")) {
			complainantBean = complaintService.copyComplainant(userCurrent);

			mv.addObject("complainantBean", complainantBean);

			/* mv.setViewName("document"); */
			mv.setViewName("complainant");

			return mv;
		}

		else if (userCurrent.getComplainantType().equalsIgnoreCase("o"))

		{

			mv.addObject("complaintDto", complaintDto);
			mv.setViewName("complaint-by-office");

			return mv;

		}

		else {
			repersentiveBean = complaintService.copyRepersentive(userCurrent);
			mv.addObject("repersentiveBean", repersentiveBean);

			mv.setViewName("representative");

			return mv;
		}

	}

	@PostMapping("/complaint_save")
	public String Complaint_Save(@RequestParam String action,
			@Valid @ModelAttribute("complainantBean") ComplainantBean complaintBean, BindingResult br,
			Authentication authentication, Model model) {

		complaintDto = complaintService.complaintentSaveProcess(complaintBean);

		if (!br.hasErrors()) {

			if (action.equalsIgnoreCase("Save As Draft")) {
				Long refno = complaintDAO.complaintSaveAsDraft(complaintDto);
				User userCurrent = complaintService.currentUserForLogin(authentication.getName());
				List<ComplaintDto> complaintDto = complaintDAO.findByCompEmail(authentication.getName());
				model.addAttribute("user", userCurrent);
				model.addAttribute("dto", complaintDto);
				model.addAttribute("refno", refno);
				/* return "user-DashBoard"; */
				return "redirect:/deshBorad";
			}

			else if (action.equals("Save And Next")) {

				Long refno = complaintDAO.complaintSaveAsDraft(complaintDto);
				model.addAttribute("refno", refno);
				return "document";
			}

		}
		return "error";

	}

	@PostMapping("/complaint_save_rep")
	public String Complaint_Save(@RequestParam String action,
			@Valid @ModelAttribute("repersentiveBean") RepersentiveBean repersentiveBean, BindingResult br,
			Authentication authentication, Model model) {
		complaintService.repersentiveSaveProcess(repersentiveBean);

		if (!br.hasErrors()) {

			if (action.equalsIgnoreCase("Save As Draft")) {
				Long refno = complaintDAO.complaintSaveAsDraft(complaintDto);
				User userCurrent = complaintService.currentUserForLogin(authentication.getName());
				List<ComplaintDto> complaintDto = complaintDAO.findByReperEmail(authentication.getName());
				model.addAttribute("user", userCurrent);
				model.addAttribute("dto", complaintDto);
				model.addAttribute("refno", refno);
				/* return "user-DashBoard"; */
				return "redirect:/deshBorad";
			}

			else if (action.equals("Save And Next")) {
				Long refno = complaintDAO.complaintSaveAsDraft(complaintDto);
				model.addAttribute("refno", refno);
				return "document";
			}

		}
		return "error";

	}

	@PostMapping("/complaint_by_office")
	public String Complaint_Save(@RequestParam String action,
			@Valid @ModelAttribute("complaintDto") ComplaintDto complaintDto, BindingResult br,
			Authentication authentication, Model model) {

		if (!br.hasErrors()) {
			if (action.equalsIgnoreCase("Save As Draft")) {
				complaintDto.setComplainantType("o");
				complaintDto.setComplaintStatus("Upload document is pending");
				Long refno = complaintDAO.complaintSaveAsDraft(complaintDto);
				User userCurrent = complaintService.currentUserForLogin(authentication.getName());
				List<ComplaintDto> clist = complaintDAO.findByOffice();
				model.addAttribute("user", userCurrent);
				model.addAttribute("dto", clist);
				model.addAttribute("refno", refno);
				/* return "user-DashBoard"; */
				return "redirect:/deshBorad";

			}

			else if (action.equals("Save And Next")) {

				complaintDto.setComplainantType("o");
				complaintDto.setComplaintStatus("In Process");
				Long refno = complaintDAO.complaintSaveAsDraft(complaintDto);
				User userCurrent = complaintService.currentUserForLogin(authentication.getName());

				model.addAttribute("user", userCurrent);
				model.addAttribute("dto", complaintDto);

				model.addAttribute("refno", refno);
				return "document";
			}

		}
		return "error";

		/*
		 * if (br.hasErrors()) {
		 * 
		 * return "error"; }
		 * 
		 * complaintDto.setComplainantType("o");
		 * complaintDto.setComplaintStatus("In process");
		 * complaintDAO.complaintSaveAsDraft(complaintDto); return "document";
		 */
	}

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public ModelAndView displayRegistration(ModelAndView modelAndView, User user) {
		List<State> statelist = complaintDAO.stateData();
		modelAndView.addObject("stateList", statelist);

		List<DisablityPercentage> perList = complaintDAO.disablityPercentage();
		modelAndView.addObject("perList", perList);

		List<Disability> disabilities = complaintDAO.disablityData();
		modelAndView.addObject("disability", disabilities);

		modelAndView.addObject("user", user);

		modelAndView.setViewName("register");
		return modelAndView;
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ModelAndView registerUser(ModelAndView modelAndView, User user) {
		String pwd = user.getPassword();
		String encryptpwd = passwordEncoder.encode(pwd);
		user.setPassword(encryptpwd);
		User existingUser = userServices.checkUserByEmail(user.getEmail());

		if (existingUser != null) {
			modelAndView.addObject("message", "This email already exists!");
			modelAndView.setViewName("error");
		} else {

			userServices.saveProcessUser(user);

			ConfirmationToken confirmationToken = new ConfirmationToken(user);
			userServices.newUserToken(confirmationToken);

			SimpleMailMessage mailMessage = new SimpleMailMessage();
			mailMessage.setTo(user.getEmail());
			mailMessage.setSubject("Complete Registration!");
			mailMessage.setText("To confirm your account, please click here : "
					+ "http://localhost:8080/confirm-account?token=" + confirmationToken.getConfirmationToken());

			emailService.sendEmail(mailMessage);

			modelAndView.addObject("email", user.getEmail());

			modelAndView.setViewName("successfulRegisteration");
		}

		return modelAndView;
	}

	@RequestMapping(value = "/confirm-account", method = RequestMethod.GET)
	public ModelAndView confirmUserAccount(ModelAndView modelAndView, @RequestParam("token") String confirmationToken) {

		String result = userServices.confirmUserAccount(confirmationToken);

		if (result.equalsIgnoreCase("accountVerified")) {

			modelAndView.setViewName("accountVerified");

		} else {
			modelAndView.addObject("message", result);
			modelAndView.setViewName("error");
		}

		return modelAndView;
	}

	@PostMapping("/register1")
	public String registration(@Valid @ModelAttribute("user") User user, BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			return "registration";
		}
		/* securityService.autoLogin(user.getUserName(), user.getPassword()); */

		userServices.saveProcessUser(user);

		return "redirect:/welcome";
	}

	/* check if file is uploaded redircet deshboard else go document page */

	/*
	 * @GetMapping("uploadDraft/{id}") public String
	 * showUpdateForm(@PathVariable("id") Long id, Model model) {
	 * 
	 * // List<DBFile> dbFileTemp = dbFileRepository.findByReferenceNo(id);
	 * 
	 * 
	 * DBFile dbFileTemp =
	 * dbFileRepository.findByRefeAndDocDesc(Integer.valueOf(id), "DC"); if
	 * (dbFileTemp != null) { return "redirect:/deshBorad";
	 * 
	 * } else if(dbFileTemp.equals(null)) //model.addAttribute("docId",
	 * dbFileTemp.getId()); model.addAttribute("refno", id);
	 * 
	 * 
	 * manish 1-2 List<DBFile> dbFileTemp = dbFileRepository.findByReferenceNo(id);
	 * 
	 * for (DBFile list : dbFileTemp) { if
	 * (list.getDocumentDesc().equalsIgnoreCase("Disability Certificate")) {
	 * 
	 * model.addAttribute("msg", "Document is Already Uploaded!"); return
	 * "redirect:/deshBorad"; }
	 * 
	 * } model.addAttribute("refno", id); return "document"; }
	 */
	
	@GetMapping("uploadDraft/{id}")
	public String showUpdateForm(@PathVariable("id") String id, Model model) {

	DBFile dbFileTemp = dbFileRepository.findByRefeAndDocDesc(Long.valueOf(id),"DC");
	if(dbFileTemp!=null)
	{
	model.addAttribute("docId", dbFileTemp.getId());
	}
	List<DBFile> dbFilesList = dbFileRepository.findByRefeAndDocDescList(Long.valueOf(id),"OD");

	model.addAttribute("dbFilesList", dbFilesList);
	if(dbFilesList!=null && dbFilesList.size()>0)
	{
	model.addAttribute("dbFilesListSize", Boolean.TRUE);
	}
	else
	{
	model.addAttribute("dbFilesListSize", Boolean.FALSE);
	}
	model.addAttribute("refno", id);
	        return "document";
	}

}
