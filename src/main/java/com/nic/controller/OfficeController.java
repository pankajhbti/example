package com.nic.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.nic.dao.BacKOfficeDAO;
import com.nic.model.BackOfficeUser;
import com.nic.model.Designation;
import com.nic.service.BackOfficeService;

@Controller
public class OfficeController {

	@Autowired
	BackOfficeService backOfficeService;
	@Autowired
	BacKOfficeDAO bacKOfficeDAO;

	@RequestMapping("/office/")
	public ModelAndView office() {
		return new ModelAndView("office-login");
	}

	@RequestMapping("/office/login")
	public ModelAndView officelogin() {
		return new ModelAndView("office-login");

	}

	@RequestMapping("/office/dashboard")
	public ModelAndView officedashboard(Authentication authentication, Model model) {
		BackOfficeUser officeUser = backOfficeService.checkByEmail(authentication.getName());
		if (officeUser.getDesignationId() == 1) {
			List<BackOfficeUser> userList = backOfficeService.findAllUser();
			model.addAttribute("userList", userList);
			model.addAttribute("currentUser", officeUser);
			return new ModelAndView("deskOfficer");
		}
		return new ModelAndView("deskOfficer");

	}

	@RequestMapping("/office/accessdenied")
	public ModelAndView officeAccessError() {
		return new ModelAndView("office-accessdenied");
	}

	@GetMapping("/nic/ccpd/back/office/user/registraion")
	public String backOfficeReg(ModelAndView mv, BackOfficeUser backOfficeUser, Model model) {
		List<Designation> designation = bacKOfficeDAO.getAllDesignationListDesignation();

		model.addAttribute("designation", designation);
		mv.addObject(backOfficeUser);
		return "back-office-regisration";

	}

	@PostMapping("/nic/ccpd/back/office/user/registraion")
	public String backOfficeRegSubmit(ModelAndView mv, BackOfficeUser backOfficeUser, Model model) {

		BackOfficeUser officeUser = backOfficeService.checkByEmail(backOfficeUser.getEmail());

		if (officeUser != null) {
			List<Designation> designation = bacKOfficeDAO.getAllDesignationListDesignation();
			model.addAttribute("designation", designation);
			model.addAttribute("msg", "This email already exists!");

			return "back-office-regisration";

		} else

		{
			List<Designation> designation = bacKOfficeDAO.getAllDesignationListDesignation();
			model.addAttribute("designation", designation);
			BackOfficeUser saveUser = backOfficeService.saveProcessOffice(backOfficeUser);

			String msg = "Your Account Register Please Wait For Activation :" + saveUser.getEmail();
			System.out.println(saveUser.getEmail());
			model.addAttribute("msg", msg);

			return "officeWelcomeMsg";

		}
	}

	@GetMapping("/admin/userView/{userId}")
	public String userView(@PathVariable("userId") Long userId, Model model) {

		BackOfficeUser user = backOfficeService.getUserDetails(userId);

		model.addAttribute("user", user);
		return "office-User-Detials";

	}

	@GetMapping("admin/isEnable/{userId}")
	public String activation(@RequestParam String action, @PathVariable("userId") Long userId) {
		System.out.println(userId + " isenable");
		if (action.equalsIgnoreCase("Active")) {
			System.out.println(action);
			BackOfficeUser activeUser = backOfficeService.isEnable(userId);
			return "redirect:/office/dashboard";
		} else {
			System.out.println(action);
			BackOfficeUser deactiveUser = backOfficeService.isDeactive(userId);
			return "redirect:/office/dashboard";
		}
	}

}
