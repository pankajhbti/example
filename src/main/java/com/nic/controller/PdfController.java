package com.nic.controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.file.FileSystems;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.context.WebContext;
import org.w3c.tidy.Tidy;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.text.DocumentException;
import com.nic.dao.ComplaintDAO;
import com.nic.dto.ComplaintDto;
import com.nic.model.ComplaintPdfBean;
import com.nic.model.DBFile;
import com.nic.repo.ComplaintRepo;
import com.nic.repo.StateRepository;
import com.nic.service.PdfService;

@Controller
public class PdfController {
	private static final String UTF_8 = "UTF-8";
	@Autowired
	ServletContext servletContext;
	@Autowired
	ComplaintDAO complaintDAO;
	@Autowired
	ComplaintDto complaintDto;
	@Autowired
	StateRepository stateRepository;
	@Autowired
	PdfService dfService;
	@Autowired
	PdfService pdfService;
	@Autowired
	ComplaintPdfBean complaintPdfBean;

	@Autowired
	ComplaintRepo complaintRepo;

	private final TemplateEngine templateEngine;

	public PdfController(TemplateEngine templateEngine) {
		this.templateEngine = templateEngine;
	}
	/*
	 * private final TemplateEngine templateEngine;
	 * 
	 * public PdfController(TemplateEngine templateEngine) {
	 * 
	 * this.templateEngine = templateEngine; }
	 */

	@GetMapping("viewComplaint/{refno}")
	public String viewComplaint(@PathVariable("refno") Long refno, Model model) {

		complaintDto = complaintDAO.findByRefNo(refno);

		List<DBFile> dbFileTemp = complaintDAO.listOfFileByUser(refno);

		if (complaintDto.getComplainantType().equalsIgnoreCase("o")) {
			complaintPdfBean = pdfService.getComplaintInfo(complaintDto);
			model.addAttribute(complaintPdfBean);
			model.addAttribute("list", dbFileTemp);
			return "complant-privew";
		}

		complaintPdfBean = pdfService.getComplaintInfo(complaintDto);

		model.addAttribute(complaintPdfBean);
		model.addAttribute("list", dbFileTemp);

		return "complant-privew";

		/*
		 * @RequestMapping("/genratePdf/{refno}") public ResponseEntity<?>
		 * genratePdf(@PathVariable("refno") Integer refno, HttpServletRequest request,
		 * HttpServletResponse response) {
		 * 
		 * complaintDto = complaintDAO.findByRefNo(refno);
		 * 
		 * complaintPdfBean = pdfService.getComplaintInfo(complaintDto);
		 * 
		 * WebContext context = new WebContext(request, response, servletContext);
		 * 
		 * context.setVariable("complaintPdfBean", complaintPdfBean); String
		 * grievanceHtml = templateEngine.process("complant-privew", context);
		 * ByteArrayOutputStream target = new ByteArrayOutputStream();
		 * ConverterProperties converterProperties = new ConverterProperties();
		 * 
		 * set url converterProperties.setBaseUri("http://localhost:8080");
		 * 
		 * HtmlConverter.convertToPdf(grievanceHtml, target, converterProperties);
		 * byte[] bytes = target.toByteArray();
		 * 
		 * return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
		 * "attachment; filename=grievance.pdf")
		 * .contentType(MediaType.APPLICATION_PDF).body(true);
		 * 
		 * }
		 */
	}

	
	  @RequestMapping("download-pdf/{refno}") 
	  public ResponseEntity<?>getPDF(HttpServletRequest request, HttpServletResponse
	  response,@PathVariable("refno") Long refno) throws IOException {
	  
	  /*Do Business Logic */
	  System.out.println(refno+" "+"downlodadaf"); 
	  complaintDto =complaintRepo.findById(refno).orElse(null); 
	  complaintPdfBean = pdfService.getComplaintInfo(complaintDto);
	  System.out.println(complaintDto.getcMobile()+" dfdf      "+complaintDto.getRefno()); 
	  WebContext context = new WebContext(request, response, servletContext);
	  context.setVariable("complaintPdfBean", complaintPdfBean); 
	  String grievanceHtml = templateEngine.process("complant-privew", context);
	  System.out.println("grievanceHtml" + grievanceHtml);
	  ByteArrayOutputStream target = new ByteArrayOutputStream();
	  ConverterProperties converterProperties = new ConverterProperties();
	  converterProperties.setBaseUri("http://localhost:8080");
	  HtmlConverter.convertToPdf(grievanceHtml, target,converterProperties);
	  byte[] bytes = target.toByteArray();
	  
	  return ResponseEntity.ok() .header(HttpHeaders.CONTENT_DISPOSITION,"inline; filename=complantprivew.pdf")
	                   .contentType(MediaType.APPLICATION_PDF) .body(bytes);
	  
	  }
	 

	/*
	 * @GetMapping("download-pdf/{refno}") public void
	 * downloadPDFResource(@PathVariable("refno") Long refno,HttpServletResponse
	 * response) { try {
	 * 
	 * System.out.println(refno+" "+"downlodadaf"); Path file =
	 * Paths.get(complaintPdfService.generatePdf(refno).getAbsolutePath()); if
	 * (Files.exists(file)) { response.setContentType("application/pdf");
	 * response.addHeader("Content-Disposition", "attachment; filename=" +
	 * file.getFileName()); Files.copy(file, response.getOutputStream());
	 * response.getOutputStream().flush(); } } catch (DocumentException |
	 * IOException ex) { ex.printStackTrace(); } }
	 */
	
	

	 
}