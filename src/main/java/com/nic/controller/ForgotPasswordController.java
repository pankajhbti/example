package com.nic.controller;

import java.io.UnsupportedEncodingException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.nic.exception.UserNotFoundException;
import com.nic.model.User;
import com.nic.service.UserServices;
import com.nic.utility.Utility;

import net.bytebuddy.utility.RandomString;

@Controller
public class ForgotPasswordController {

	@Autowired
	Utility utility;

	@Autowired
	JavaMailSender mailSender;

	@Autowired
	UserServices userServices;

	@GetMapping("/forgot_password")
	public String showForgotPasswordForm(Model model) {

		return "forgot-password-form";

	}

	@PostMapping("/forgot_password")

	public String processForgotPassword(HttpServletRequest request, Model model) {
		String email = request.getParameter("email");
		String token = RandomString.make(30);

		try {

			userServices.updateResetPasswordToken(token, email);
			String resetPasswordLink = utility.getSiteURL(request) + "/reset_password?token=" + token;
			System.out.println(resetPasswordLink);
			sendEmail(email, resetPasswordLink);
			model.addAttribute("message", "We have sent a reset password link to your email. Please check.");

		}

		catch (UserNotFoundException e) {
			model.addAttribute("error", e.getMessage());
		}

		catch (UnsupportedEncodingException | MessagingException e) {
			model.addAttribute("error", "Error while sending email");
		}

		model.addAttribute("pageTitle", "Forgot Password");
		return "forgot-password-form";
	}

	private void sendEmail(String email, String resetPasswordLink)
			throws UnsupportedEncodingException, MessagingException {

		MimeMessage message = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message);

		helper.setFrom("ms.pankajsingh2011@gmail.com", "CCPD Support");
		helper.setTo(email);
		String subject = "Here's the link to reset your password";

		String content = "<p>Hello,</p>" + "<p>You have requested to reset your password.</p>"
				+ "<p>Click the link below to change your password:</p>" + "<p><a href=\"" + resetPasswordLink
				+ "\">Change my password</a></p>" + "<br>" + "<p>Ignore this email if you do remember your password, "
				+ "or you have not made the request.</p>";

		helper.setSubject(subject);
		helper.setText(content, true);
		mailSender.send(message);

	}

	@GetMapping("/reset_password")
	public String showResetPasswordForm(@Param(value = "token") String token, Model model) {
		User user = userServices.getByResetPasswordToken(token);
		model.addAttribute("title", "Reset Your  Password");
		model.addAttribute("token", token);
		if (user == null) {
			model.addAttribute("message", "Invalid Token");
			return "message";
		}

		model.addAttribute("token", token);
		model.addAttribute("pageTitle", "Reset Your  Password");
		return "reset-password-form";

	}

	@PostMapping("/reset_password")
	public String processResetPassword(HttpServletRequest request, Model model) {
		String token = request.getParameter("token");
		String password = request.getParameter("password");
		User user = userServices.getByResetPasswordToken(token);
		model.addAttribute("title", "Reset Your  Password");

		if (user == null) {
			model.addAttribute("message", "Invalid Token");
			return "message";

		} else {
			userServices.updatePassword(user, password);
			model.addAttribute("message", "You have successfully changed your password.");
		}
		return "message";

	}

}
