package com.nic.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.nic.model.DBFile;
import com.nic.repo.DBFileRepository;
import com.nic.service.DBFileStorageService;
import com.nic.service.SyncSequenceGenerator;

@RestController
public class FileController {

	private static final Logger logger = LoggerFactory.getLogger(FileController.class);

	public static String uploadDirectory = "C:\\upload_documents";
	@Autowired
	private DBFileStorageService dbFileStorageService;

	@Autowired
	private DBFileRepository dbFileRepository;
	@Autowired
	SyncSequenceGenerator sequenceGenerator;

	@PostMapping("/uploadFile")
	public String uploadFile(@RequestParam("file") MultipartFile file,
			@RequestParam("documentDesc") String documentDesc, @RequestParam("refNo") Long refNo) {
		try {
			System.out.println(refNo);
			DBFile dbFile = new DBFile();
			String filename = file.getOriginalFilename();
			if (refNo != null) {
				DBFile dbFileTemp = dbFileRepository.findByRefeAndDocDesc(refNo, "DC");

				if (dbFileTemp != null) {
					dbFile.setId(dbFileTemp.getId());
				}
			}

			long j = 0;
			String directory = "C:\\upload_documents";
			/* String directory = "/Users/home"; */
			String fname = "disability-certificate";
			String filepath = Paths.get(directory, fname + "-" + refNo + "-" + j).toString();
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(filepath)));
			stream.write(file.getBytes());
			dbFile.setDocumentDesc(documentDesc);
			dbFile.setFileDownloadUri(filepath);
			dbFile.setTimeStamp(new Date());
			dbFile.setReferenceNo(refNo);
			dbFile.setFileName(fname);
			dbFile.setSerialNo(j);
			dbFile.setDocsType("DC");
			if (file.getOriginalFilename().lastIndexOf(".") != -1 && file.getOriginalFilename().lastIndexOf(".") != 0) {
				dbFile.setFileExten(
						file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1));
			}
			dbFileRepository.save(dbFile);
			return dbFile != null ? dbFile.getId().toString() : null;

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	@PostMapping("/uploadMultipleFiles")
	public String uploadMultipleFiles(@RequestParam("files") MultipartFile files,
			@RequestParam("documentDesc") String documentDesc, @RequestParam("refNo") Long refNo,@RequestParam("uploadNumber") Long uploadNumber) {
		try {
			System.out.println(uploadNumber);
			DBFile dbFile = new DBFile();
			String filename = files.getOriginalFilename();
			System.out.println();
			if (refNo != null) {
				DBFile dbFileTemp = dbFileRepository.findByRefeAndDocDescForMulti(refNo, "OD",uploadNumber);
				if (dbFileTemp != null) {
					dbFile.setId(dbFileTemp.getId());
				}
			}

			//long i = sequenceGenerator.getNext();
			//System.out.println(uploadNumber);

			String directory = "C:\\upload_documents";
			/* String directory = "/Users/home"; */
			/* String directory = "F:\\CCPD image"; */
			/* String directory = "/Users/home"; */
			String fname = "supporting-documents";
			/*
			 * Integer index = 0; for (int i = 0; index < files.length; index++) { index =
			 * index + 1; }
			 */String filepath = Paths.get(directory, fname + "-" + refNo + "-" + uploadNumber).toString();
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(filepath)));
			stream.write(files.getBytes());
			dbFile.setDocumentDesc(documentDesc);
			dbFile.setFileDownloadUri(filepath);
			dbFile.setTimeStamp(new Date());
			dbFile.setReferenceNo(refNo);
			dbFile.setFileName(fname);
			dbFile.setSerialNo(uploadNumber);
			dbFile.setDocsType("OD");
			if (files.getOriginalFilename().lastIndexOf(".") != -1
					&& files.getOriginalFilename().lastIndexOf(".") != 0) {
				dbFile.setFileExten(
						files.getOriginalFilename().substring(files.getOriginalFilename().lastIndexOf(".") + 1));
			}
			dbFileRepository.save(dbFile);
			return dbFile != null ? dbFile.getId().toString() : null;

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return null;
	}

//	@GetMapping("/downloadFile/{fileId}")
//	public ResponseEntity<Resource> downloadFile(@PathVariable Long fileId) {
//		// Load file from database
//		DBFile dbFile = dbFileStorageService.getFile(fileId);
//		System.out.println(" hii" + dbFile);
//		byte[] data = null;
//
//		Path path = Paths.get(uploadDirectory + "/" + dbFile.getFileName());
//		try {
//			data = Files.readAllBytes(path);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return ResponseEntity.ok().contentType(MediaType.parseMediaType(dbFile.getFileType()))
//				/*
//				 * .header("", "attachment; filename=\"" + dbFile.getFileName() + "\"")
//				 */
//				.body(new ByteArrayResource(data));
//
//	}

	@GetMapping("/display-file/{fileId}")
	public void displayFiles(@PathVariable Long fileId, HttpServletRequest request, HttpServletResponse response) {
		String filePath = null;
		DBFile dbFileTemp = null;
		try {
			dbFileTemp = dbFileRepository.findByFilePathById(fileId);
			filePath = dbFileTemp.getFileDownloadUri();

			Path file = Paths.get(filePath);
			if (Files.exists(file)) {
				response.setContentType("application/pdf");
				response.addHeader("Content-Disposition", "inline; filename=" + dbFileTemp.getFileName());
				try {
					Files.copy(file, response.getOutputStream());
					response.getOutputStream().flush();
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}

		} catch (Exception e) {
			logger.error("Exception encountered while rendering the file", e);
		}
	}

}
