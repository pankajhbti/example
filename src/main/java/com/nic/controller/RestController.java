package com.nic.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nic.model.District;
import com.nic.model.SubDepartment;
import com.nic.model.SubOrg;
import com.nic.repo.DistrictRepository;
import com.nic.repo.SubDepartRepositry;
import com.nic.repo.SubOrgRepositry;

@org.springframework.web.bind.annotation.RestController
public class RestController {

	@Autowired
	private DistrictRepository districtRepository;
	
	@Autowired
	SubOrgRepositry subOrgRepositry;

	@Autowired
	SubDepartRepositry subDepartRepositry;



	@RequestMapping(value = "/districtOnState", method = RequestMethod.GET)
	public @ResponseBody List<District> districtOnState(@RequestParam("stateId") Integer stateId) {

		return districtRepository.findByStatecodeid(stateId);
	}

	@RequestMapping(value = "/districtOnState2", method = RequestMethod.GET)
	public @ResponseBody List<District> districtOnState2(@RequestParam("stateId2") Integer stateId2) {

		List<District> dlist = districtRepository.findByStatecodeid(stateId2);

		return dlist;
	}



	@RequestMapping(value = "/subOrgOnOrg", method = RequestMethod.GET)
	public @ResponseBody List<SubOrg> subOrgOnOrg(@RequestParam("orgId") Integer orgId) {

		List<SubOrg> list = subOrgRepositry.findByOrgcode(orgId);

		return list;

	}

	@RequestMapping(value = "/subDepartment", method = RequestMethod.GET)
	public @ResponseBody List<SubDepartment> subDepartment(@RequestParam("suborgCode") Integer suborgCode) {

		System.out.println("subDepartment" + suborgCode);
		List<SubDepartment> list = subDepartRepositry.findBysubOrgCode(suborgCode);

		return list;

	}

}
