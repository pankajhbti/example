package com.nic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CCPD {

	public static void main(String[] args) {
		SpringApplication.run(CCPD.class, args);
	}

}
