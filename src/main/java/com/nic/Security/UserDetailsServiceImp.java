package com.nic.Security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.nic.model.User;
import com.nic.repo.UserRepository;

public class UserDetailsServiceImp implements UserDetailsService {

	@Autowired
	UserRepository userRepositry;
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		System.out.println(username);
		User user= userRepositry.findByEmail(username);
		if (user == null) {
			throw new UsernameNotFoundException("User not found");
		}
		
		
		return new UserDetailSecurity(user);
	}

}
