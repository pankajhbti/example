package com.nic.Security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.nic.model.BackOfficeUser;
import com.nic.repo.BackOfficeUserRepo;

public class OfficeDetailsServiceImp implements UserDetailsService {

	@Autowired
	BackOfficeUserRepo backOfficeUserRepo;
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		System.out.println(username);
		BackOfficeUser backOfficeUser = backOfficeUserRepo.findByemail(username);
		if(backOfficeUser != null && backOfficeUser.isIsenabled() == true)
		{
			return new OfficeDetailsSecurity(backOfficeUser);
			
		}
		
		
		throw new UsernameNotFoundException("User not found");
	}

}
