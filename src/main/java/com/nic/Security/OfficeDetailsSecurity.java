package com.nic.Security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.nic.model.BackOfficeUser;

public class OfficeDetailsSecurity implements UserDetails {

	
	private BackOfficeUser backOfficeUser;
	
	
	
	public OfficeDetailsSecurity(BackOfficeUser backOfficeUser) {
		super();
		this.backOfficeUser = backOfficeUser;
		
	}
	
	

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return backOfficeUser.getUserPassword();
	}

	@Override
	public String getUsername() {
		
		return backOfficeUser.getEmail();
		
		
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		
		
		return true;
	}

}
