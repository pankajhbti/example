package com.nic.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.nic.Security.UserDetailsServiceImp;
@Order(2)
@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private DataSource dataSource;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	@Autowired 
	private UserDetailsService userDetailsService;
	 
	
	@Bean
	public UserDetailsService userDetailsService()
	{
		return new UserDetailsServiceImp() ;
		
	}
	
	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(userDetailsService());
		authProvider.setPasswordEncoder(bCryptPasswordEncoder);
		
		return authProvider;
	}
	
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authenticationProvider());
	}

	
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		/* Manish */
		http
		
		.authorizeRequests()
		.antMatchers("/images/**").permitAll()
		.antMatchers("/css/**").permitAll()
		.antMatchers("/js/**").permitAll()
		.antMatchers("/resources/**").permitAll()
		.antMatchers("/nic/ccpd/back/office/user/registraion").permitAll()
		.antMatchers("/register","/districtOnState","/forgot_password","/reset_password","/subOrgOnOrg").permitAll()
		.anyRequest().authenticated() 
		.and()
		.formLogin()
		.loginPage("/login").permitAll()
		.and()
		.logout().invalidateHttpSession(true)
		.clearAuthentication(true)
		.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
		.logoutSuccessUrl("/logout-success").permitAll();
		http.csrf().disable();
		
		/*  runing Pankaj
		 * http.authorizeRequests()
		 * .antMatchers("/user_registration_details").authenticated()
		 * .anyRequest().permitAll().and().formLogin().usernameParameter("username")
		 * .defaultSuccessUrl("/welcomePage").permitAll()
		 * .passwordParameter("password").failureUrl("/login?errroooooooor");
		 */
	
	}


	
	
}


