package com.nic.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.nic.Security.OfficeDetailsServiceImp;

@Order(1)
@Configuration
@EnableWebSecurity

public class OfficeSecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	private DataSource dataSource;
	@Autowired
	private UserDetailsService userDetailsService;

	@Bean
	public UserDetailsService officUserDetailsService() {
		return new OfficeDetailsServiceImp();

	}

	@Bean
	public DaoAuthenticationProvider authenticationProvideroffice() {
		DaoAuthenticationProvider authProvideroffice = new DaoAuthenticationProvider();

		authProvideroffice.setUserDetailsService(officUserDetailsService());
		authProvideroffice.setPasswordEncoder(bCryptPasswordEncoder);
		return authProvideroffice;

	}

	
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authenticationProvideroffice());
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {

		http
		.antMatcher("/office/**")
		.authorizeRequests().antMatchers("/images/**").permitAll()
				.antMatchers("/css/**").permitAll()
				.antMatchers("/js/**").permitAll()
				.antMatchers("/resources/**").permitAll()
				.antMatchers("/nic/ccpd/back/office/user/registraion").permitAll()
				.antMatchers("/register", "/districtOnState", "/forgot_password", "/reset_password", "/subOrgOnOrg")
				.permitAll().and().formLogin().loginPage("/office/login").defaultSuccessUrl("/office/dashboard", true)

				.permitAll().and().logout().logoutUrl("/office/logout").logoutSuccessUrl("/office/login").and()
				.exceptionHandling().accessDeniedPage("/office/accessdenied");
		http.csrf().disable();

	}

}
